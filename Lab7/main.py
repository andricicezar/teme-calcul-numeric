import os
import time
import json
from Lab7 import run1234
from utils.utils import *
from flask_cors import CORS
from flask import Flask, request, redirect, render_template, jsonify


app = Flask(
    __name__,
    # os.path.abspath(os.path.join(os.curdir, 'public')),
    # static_folder='public',
    # template_folder=os.path.abspath(os.path.join(os.curdir, 'templates'))
)

settings = settings_init()
app.template = dict(
    subtitle='Tema 7 - Bugeag George Tiberiu & Andrici Cezar Constantin',
    post_address=settings['address'] + ':' + settings['port']
)
app.template_folder = os.path.join(app.root_path, settings['paths']['templates'])
app.static_folder = os.path.join(app.root_path, settings['paths']['static'])
CORS(app)


def clean_expired_saves():
    save_files_path = os.path.join(app.root_path, settings['paths']['saves'])
    for file in os.listdir(os.path.join(app.root_path, settings['paths']['saves'])):
        if file.endswith('.json'):
            with open(os.path.join(save_files_path, file), 'r') as fh:
                file_dict = json.load(fh)
            if file_dict.get('expiration') is None or file_dict['expiration'] < time.time():
                os.unlink(os.path.join(save_files_path, file))


@app.context_processor
def inject_template_rendered():
    return dict(template=app.template or dict(
        name='base.html',
        subtitle='Tema 7 - Bugeag George Tiberiu & Andrici Cezar Constantin',
        post_address=settings['address'] + ':' + settings['port']
    ))


@app.route('/')
@app.route('/index')
def redirect_to_home():
    return redirect('/home')


@app.route('/problema1', methods=['GET'])
def problema1():
    app.template['name'] = 'problema1.html'
    return render_template(app.template['name'])


@app.route('/problema1/solution', methods=['POST'])
def solve_poly():
    try:
        # print(request.form['x'])
        result = run1234(
            [float(x) for x in request.form['x'].split(',')],
            [float(x) for x in request.form['fx'].split(',')],
            float(request.form['xbarat']),
            float(request.form['dl']),
            float(request.form['dr']),
        )
        return jsonify(result)
    except ValueError as e:
        print(e)
        return jsonify({'error': 'Invalid request! ' + str(e)}), 400


@app.route('/home')
def home_page():
    app.template['name'] = 'index.html'
    return render_template(app.template['name'])


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('-a', '--host', type=str, help='Address(IP/DNS) at which the node will be found')
    parser.add_argument('-p', '--port', type=int, help='Port on which the node will listen')
    args = parser.parse_args()
    host = args.host or settings['address']
    port = args.port or settings['port']
    app.run(host=host, port=port, debug=True, threaded=True)
