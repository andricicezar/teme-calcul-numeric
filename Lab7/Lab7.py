import json
import numpy as np


def multiply(iterable):
    prod = 1
    for x in iterable:
        prod *= x
    return prod


def default_func(point):
    return pow(point, 3) + 3 * pow(point, 2) - 5 * point + 12


class PolyLagrange(object):
    def __init__(self, point_values, function_values: list = None, precision: int = None, control_function=None):
        if control_function and isinstance(control_function, float):
            self.control_function = None
            self.control_function_values = None
            self.control_value = control_function
        elif control_function:
            self.control_function = control_function
            self.control_function_values = [self.control_function(pct) for pct in point_values]
            self.control_value = None
        elif control_function is None and function_values:
            self.control_function = None
            self.control_function_values = None
            self.control_value = None

        self.epsilon = pow(10, -1 * (precision or 6))

        if len(point_values) < 2:
            raise ValueError('There must be at least 2 points!')

        if function_values and len(function_values) != len(point_values):
            raise ValueError('Function values number should be the same with the number of points! One to one parity!')

        if function_values is None and self.control_function is None:
            raise ValueError('You must provide the result of the function for given points or the function itself!')

        self.poly_table = {
            'pct': point_values,
            'fct': function_values or self.control_function_values
        }
        self._div_diffs = dict()
        self.solution = dict()

    def compute_lagrange(self, point):
        the_sum = 0
        point_nr = len(self.poly_table['pct']) - 1
        for idx in range(point_nr):
            the_sum += self.poly_table['fct'][idx] * multiply([
                (point - self.poly_table['pct'][pos]) / (self.poly_table['pct'][idx] - self.poly_table['pct'][pos])
                for pos in range(point_nr) if pos != idx
            ])
        return the_sum

    def approximate_lagrange(self, point):
        n_length = len(self.poly_table['pct']) - 1
        lagrange = self.poly_table['fct'][0]
        for idx in range(1, n_length):
            lagrange += self.get_diff_div(0, idx) * multiply([point - elem for elem in self.poly_table["pct"][:idx]])
        return lagrange

    def _compute_diff(self, start, end):
        if end < start or end == start:
            raise ValueError('End point must always be bigger with at least 1 than start point!')
        if start < 0 or start > len(self.poly_table["pct"]) - 2:
            raise ValueError(f'Start point must be between in the [0, {len(self.poly_table["pct"]) - 2}] interval!')
        if end < 1 or end > len(self.poly_table["pct"]) - 1:
            raise ValueError(f'End point must be in the [1, {len(self.poly_table["pct"]) - 1}] interval!')
        if end - start == 1:
            if self._div_diffs is None:
                self._div_diffs[f'x{start} to x{end}'] = \
                    (self.poly_table['fct'][end] - self.poly_table['fct'][start]) / \
                    (self.poly_table['pct'][end] - self.poly_table['pct'][start])
            elif self._div_diffs.get(f'x{start} to x{end}') is None:
                self._div_diffs.update({
                    f'x{start} to x{end}':
                    (self.poly_table['fct'][end] - self.poly_table['fct'][start]) /
                    (self.poly_table['pct'][end] - self.poly_table['pct'][start])
                })
        else:
            if self._div_diffs is None:
                self._div_diffs[f'x{start} to x{end}'] = \
                    (self.get_diff_div(start + 1, end) - self.get_diff_div(start, end - 1)) / \
                    (self.poly_table['pct'][end] - self.poly_table['pct'][start])
            elif self._div_diffs.get(f'x{start} to x{end}') is None:
                self._div_diffs.update({
                    f'x{start} to x{end}':
                    (self.get_diff_div(start + 1, end) - self.get_diff_div(start, end - 1)) /
                    (self.poly_table['pct'][end] - self.poly_table['pct'][start])
                })
        return self._div_diffs[f'x{start} to x{end}']

    @property
    def diff_table(self):
        the_dict = dict()
        for key in sorted(self._div_diffs):
            if int(key[1:key.index(' to ')]) + 1 == int(key[key.index(' x') + 2:]):
                new_key = '[' + key.replace(' to ', ',') + ']'
            else:
                new_key = '[' + key.replace(' to ', ',...,') + ']'
            the_dict.update({new_key: self._div_diffs[key]})
        return the_dict

    @property
    def diff_dict(self):
        return {'[' + key + ']': self._div_diffs[key] for key in sorted(self._div_diffs)}

    def get_diff_div(self, start, end):
        if self._div_diffs is not None:
            if self._div_diffs.get(f'x{start} to x{end}') is None:
                return self._compute_diff(start, end)
            return self._div_diffs.get(f'x{start} to x{end}')
        else:
            return self._compute_diff(start, end)

    @property
    def lagrange_control(self):
        if self.solution and all(needed in self.solution for needed in ['control', 'first', 'newton']):
            # print(f'Solution until check is:', json.dumps(self.solution, indent=2), sep='\n')
            self.solution['|Ln - f| - Newton'] = abs(self.solution['newton'] - self.solution['control'])
            self.solution['newton_check'] = self.solution['|Ln - f| - Newton'] < self.epsilon
            self.solution['|Ln - f| - First Form'] = abs(self.solution['first'] - self.solution['control'])
            self.solution['first_check'] = self.solution['|Ln - f| - First Form'] < self.epsilon
            return self.solution['newton_check'], self.solution['first_check']
        else:
            raise ValueError('No solution was computed yet!')

    def get_solution(self, point, control=None, pretty_print: bool = None):
        if point in self.poly_table['pct']:
            control = self.poly_table['fct'][self.poly_table['pct'].index(point)]
        if all(elm is None for elm in [self.control_value, self.control_function, control]):
            raise ValueError('A control value or function needs to be provided!')
        elif control and isinstance(control, float):
            self.solution['control'] = control
            self.control_value = control
            self.control_function = None
            self.control_function_values = self.poly_table['fct']
        elif control:
            self.control_function = control
            self.solution['control'] = self.control_function(point)
            self.control_value = self.control_function(point)
            self.control_function_values = [self.control_function(pct) for pct in self.poly_table['pct']]
        elif self.control_function is not None:
            self.solution['control'] = self.control_function(point)
            self.solution['true_Y'] = [self.control_function(pct) for pct in self.poly_table['pct']]
        elif self.control_value is not None:
            self.solution['control'] = self.control_value
            self.solution['true_Y'] = None
        else:
            raise ValueError(
                'Polynomial was initiated without a control value or function and none were give for check at solution!'
            )
        self.solution.update({
            'X': self.poly_table['pct'],
            'Y': self.poly_table['fct'],
            'true_Y': self.control_function_values,
            'first': self.compute_lagrange(point),
            'newton': self.approximate_lagrange(point),
            'diff_table': self.diff_table,
        })
        checks = self.lagrange_control
        # self.solution['hmm'] = self.hmm
        # if all(check is True for check in self.lagrange_control):
        #     print('LaGrange solutions are all correct and accurate!')
        # else:
        #     print('One or both LaGrange solutions are incorrect and inaccurate!')
        return self.solution if not pretty_print else json.dumps(self.solution, indent=4)


class CubicSpline(object):
    def __init__(self, points_list: list, derived_left: float, derived_right: float,
                 fct_values: list = None, control=None, precision=None):
        if len(points_list) < 2:
            raise ValueError('The list of points must contain at least 2 values!')

        self.sol = dict()
        if control and isinstance(control, float):
            self.control_value = control
            self.control_function = None
            self.control_function_values = None
        elif control:
            self.control_value = None
            self.control_function = control
            self.control_function_values = [control(point) for point in points_list]
        elif control is None and fct_values:
            self.control_function_values = None
            self.control_function = None
            self.control_value = None

        if fct_values and len(fct_values) != len(points_list):
            raise ValueError('Function values number should be the same with the number of points! One to one parity!')

        if fct_values is None and self.control_function is None:
            raise ValueError('You must provide the result of the function for given points or the function itself!')

        self.poly_table = {
            'pct': points_list,
            'derived_left': derived_left,
            'derived_right': derived_right,
            'fct': fct_values or self.control_function_values
        }
        self.points = points_list
        self.derived_left = derived_left
        self.derived_right = derived_right
        self.fct_values = fct_values or self.control_function_values

        self.epsilon = pow(10, -1 * (precision or 6))
        self.height_values = None
        # noinspection PyUnusedLocal
        self.height_matrix = [
            [0 for col_count in range(len(self.points))]
            for row_count in range(len(self.points))
        ]
        # noinspection PyUnusedLocal
        self.fct_vector = [0 for count in range(len(self.points))]
        self.sys_sol = None

    def compute_height_values(self):
        self.height_values = [
            self.points[idx + 1] - self.points[idx]
            for idx in range(len(self.points) - 1)
        ]

    def compute_fct_vector(self):
        if self.height_values is None:
            self.compute_height_values()
        count: int = len(self.points) - 1
        for pos in range(count + 1):
            if pos == 0:
                self.fct_vector[pos] = (self.poly_table['fct'][pos + 1] - self.poly_table['fct'][pos])
                self.fct_vector[pos] /= self.height_values[pos]
                self.fct_vector[pos] -= self.poly_table['derived_left']
            elif pos == count:
                self.fct_vector[pos] = self.poly_table['fct'][pos] - self.poly_table['fct'][pos - 1]
                self.fct_vector[pos] = self.fct_vector[pos] / self.height_values[pos - 1]
                # noinspection PyTypeChecker
                self.fct_vector[pos] = self.poly_table['derived_right'] - self.fct_vector[pos]
            else:
                self.fct_vector[pos] = \
                    (self.poly_table['fct'][pos + 1] - self.poly_table['fct'][pos]) / self.height_values[pos]
                self.fct_vector[pos] -= \
                    (self.poly_table['fct'][pos] - self.poly_table['fct'][pos - 1]) / self.height_values[pos - 1]
            self.fct_vector[pos] *= 6

    def compute_height_matrix(self):
        if self.height_values is None:
            self.compute_height_values()
        for pos in range(len(self.height_matrix)):
            if pos == 0:
                self.height_matrix[pos][pos] = 2 * self.height_values[pos]
                self.height_matrix[pos][pos + 1] = self.height_values[pos]
            elif pos == len(self.height_matrix) - 1:
                self.height_matrix[pos][pos - 1] = self.height_values[-1]
                self.height_matrix[pos][pos] = 2 * self.height_values[-1]
            else:
                self.height_matrix[pos][pos - 1] = self.height_values[pos - 1]
                self.height_matrix[pos][pos] = 2 * (self.height_values[pos - 1] + self.height_values[pos])
                self.height_matrix[pos][pos + 1] = self.height_values[pos]

    def compute_main_matrix(self):
        self.compute_height_values()
        self.compute_fct_vector()
        self.compute_height_matrix()
        self.sys_sol = np.linalg.solve(self.height_matrix, self.fct_vector)
        # self.main_matrix

    def get_closest_index(self, value):
        return [
            pos
            for pos in range(len(self.points) - 1)
            if self.points[pos] <= value <= self.points[pos + 1]
        ][0]

    def get_solution(self, value, control=None, pretty_print: bool = None):
        if not self.points[0] <= value <= self.points[-1]:
            raise ValueError(
                f'The value must be from the [{self.poly_table["pct"][0]}, {self.poly_table["pct"][0]}] interval!'
            )
        if value in self.points:
            control = self.fct_values[self.points.index(value)]
        if all(elm is None for elm in [self.control_value, self.control_function, control]):
            raise ValueError('A control value or function needs to be provided!')
        elif control and isinstance(control, float):
            self.sol['control'] = control
            self.control_value = control
            self.control_function = None
            self.control_function_values = self.poly_table['fct']
        elif control:
            self.sol['control'] = control(value)
            self.control_value = None
            self.control_function = control
            self.control_function_values = [self.control_function(pct) for pct in self.points]
        elif self.control_function is not None:
            self.sol['control'] = self.control_function(value)
            self.sol['true_Y'] = self.control_function_values
        elif self.control_value is not None:
            self.sol['control'] = self.control_value
            self.sol['true_Y'] = self.poly_table['fct']
        else:
            raise ValueError(
                'Polynomial was initiated without a control value or function and none were give for check at solution!'
            )
        self.compute_main_matrix()
        self.sol['index'] = pos = self.get_closest_index(value)
        self.sol['height_values'] = self.height_values
        self.sol['points'] = self.points
        self.sol['fct_values'] = self.fct_values
        # self.sol['f_vector'] = self.fct_vector
        # self.sol['h_matrix'] = self.height_matrix
        # self.sol['a_matrix'] = self.sys_sol.tolist()
        self.sol['hi'] = self.points[pos + 1] - self.points[pos]
        self.sol['bi'] = (
            (self.fct_values[pos + 1] - self.fct_values[pos]) / self.sol['hi'] -
            (self.sol['hi'] * (self.sys_sol[pos + 1] - self.sys_sol[pos]) / 6)
        )
        self.sol['ci'] = (
            (self.points[pos + 1] * self.fct_values[pos] - self.points[pos] * self.fct_values[pos + 1]) / self.sol['hi']
            - self.sol['hi'] * (self.points[pos + 1] * self.sys_sol[pos] - self.points[pos] * self.sys_sol[pos + 1]) / 6
        )
        self.sol['spline'] = (pow(value - self.points[pos], 3) * self.sys_sol[pos + 1] / (6 * self.sol['hi'])) + \
                             (pow(self.points[pos + 1] - value, 3) * self.sys_sol[pos] / (6 * self.sol['hi'])) + \
                             (self.sol['bi'] * value) + self.sol['ci']
        self.sol['|Sf(x) - f(x)|'] = abs(self.sol['spline'] - self.sol['control'])
        self.sol['correct'] = str(self.sol['|Sf(x) - f(x)|'] < self.epsilon)
        return self.sol if not pretty_print else json.dumps(self.sol, indent=4, )


def run1234(points_list, fct_values_list, x1, dl, dr, ctrl1=None, ctrl2=None):
    if ctrl2 is None:
        ctrl2 = 30.99760765550239227650
    if ctrl1 is None:
        ctrl1 = 30.3125
    result = {}

    the_polynomial = PolyLagrange(points_list, fct_values_list)
    # result["polynomial"] = the_polynomial.get_solution(x1, control_poly, pretty_print=False)

    the_spline = CubicSpline(points_list, dl, dr, fct_values=fct_values_list)
    # result["spline"] = the_spline.get_solution(x1, control=control_spline, pretty_print=False)

    result["graphs"] = {}
    result["graphs"]["labels"] = points_list.copy()
    result["graphs"]["f_data"] = fct_values_list.copy()

    result["graphs"]["labels"].insert(2, x1)
    result["graphs"]["f_data"].insert(2, 'NaN')

    result["graphs"]["lf_data"] = []
    result["graphs"]["ln_data"] = []
    result["graphs"]["s_data"] = []

    for x in result["graphs"]["labels"]:
        # print(f'Point is {x}')
        if x in points_list:
            control_poly = fct_values_list[points_list.index(x)]
            control_spline = fct_values_list[points_list.index(x)]
            result["graphs"]["lf_data"].append(
                PolyLagrange(points_list, fct_values_list).get_solution(x, control_poly)["first"]
            )
            result["graphs"]["ln_data"].append(
                PolyLagrange(points_list, fct_values_list).get_solution(x, control_poly)["newton"]
            )
            result["graphs"]["s_data"].append(
                CubicSpline(points_list, dl, dr, fct_values=fct_values_list).get_solution(x, control_spline)["spline"]
            )
            # print('Point is in the list')
        else:
            control_poly = ctrl1
            control_spline = ctrl2
            result.update({'polynomial': the_polynomial.get_solution(x1, control_poly)})
            result.update({'spline': the_spline.get_solution(x1, control_spline)})
            result["graphs"]["lf_data"].append(
                PolyLagrange(points_list, fct_values_list).get_solution(x1, control_poly)["first"]
            )
            result["graphs"]["ln_data"].append(
                PolyLagrange(points_list, fct_values_list).get_solution(x1, control_poly)["newton"]
            )
            result["graphs"]["s_data"].append(
                CubicSpline(points_list, dl, dr, fct_values=fct_values_list).get_solution(x1, control_spline)["spline"]
            )
    #     print(f'Control for poly is: {control_poly} and for spline is {control_spline}')
    #     print(f'Iteration for point {x} finished.')
    #     print()
    # print(json.dumps(result, indent=2))

    return result


# if __name__ == '__main__':
#     pcts_1 = list(range(6))
#     fct_vals_1 = [50, 47, -2, -121, -310, -545]
#     polynomial = PolyLagrange(pcts_1, fct_vals_1)
#     print(polynomial.get_solution(1.5, 30.3125, pretty_print=True))
#     print()
#     print()
#     spline = CubicSpline(pcts_1, 6, -244, fct_values=fct_vals_1)
#     print(spline.get_solution(1.5, control=30.99760765550239227650, pretty_print=True))
#     print()
#     print()
#     points = list(range(1, 6))
#     values = [default_func(element) for element in points]
#     polynomial = PolyLagrange(points, control_function=default_func)
#     print(polynomial.get_solution(1.7, pretty_print=True))
#     print()
#     print()
#     spline = CubicSpline(
#         points, derived_left=4, derived_right=100,
#         fct_values=[default_func(val) for val in points], control=default_func
#     )
#     print(spline.get_solution(1.7, pretty_print=True))
