import operator
import os
from functools import reduce
import numpy as np
import time
import math
import random
from utils.utils import at_pow

class Problem:
    def __init__(self, A, b, N, t):
        if len(A) > 0:
            self.A = np.array(eval(A))
        else:
            self.A = 1000000 * np.random.randn(N, N)

        if len(b) > 0:
            self.b = np.array(eval(b))
        else:
            self.b = 1000000 * np.random.randn(N)

        self.state = 'A'
        self.epsilon = 10 ** -t

    def equals(self, a, b):
        return abs(a-b) < self.epsilon


    def computeDeterminant(self):
        if self.state == 'LU':
            (N, _) = self.A.shape
            p = 1
            for i in range(0, N):
                p *= self.A[i][i]

            return p

    def canComputeLU(self):
        (N, _) = self.A.shape

        for i in range(0, N):
            if self.equals(self.A[i][i], 0):
                return False

        return True


    def computeLU(self):
        self.state = 'LU'

        (N, _) = self.A.shape

        for p in range(0, N):
            for i in range(0, p+1):
                sikp = 0
                spki = 0
                for k in range(0, i):
                    sikp += self.A[i][k] * self.A[k][p]
                    spki += self.A[p][k] * self.A[k][i]

                if i != p:
                    self.A[i][p] = (self.A[i][p] - sikp) / self.A[i][i]

                self.A[p][i] = self.A[p][i] - spki

    def computeX(self):
        (N, _) = self.A.shape

        # compute Ly = b
        y_star = np.zeros(N)

        # metoda substitutiei directe
        for i in range(0, N):
            sik0 = 0
            for k in range(0, i):
                sik0 += self.A[i][k] * y_star[k]

            y_star[i] = (self.b[i] - sik0) / self.A[i][i]

        # compute Ux = y*
        x_star = np.zeros(N)

        # metoda substitutiei inverse
        for i in range(N-1, -1, -1):
            sik0 = 0
            for k in range(i+1, N):
                sik0 += self.A[i][k] * x_star[k]

            x_star[i] = y_star[i] - sik0

        return x_star

    def _computeA_init(self):
        (N, _) = self.A.shape

        A_init = np.zeros((N,N))

        for i in range(0, N):
            for j in range(0, i+1):
                A_init[i][j] = self.A[i][j]

                for k in range(0, j):
                    A_init[i][j] += self.A[i][k] * self.A[k][j]

        for i in range(0, N):
            for j in range(i+1, N):
                for k in range(0, i+1):
                    A_init[i][j] += self.A[i][k] * self.A[k][j]

        return A_init

    def checkSolution(self, x):
        (N, _) = self.A.shape
        A_init = self._computeA_init() 

        X = np.zeros(N)

        for i in range(0, N):
            for j in range(0, N):
                X[i] += A_init[i][j] * x[j]

        X = X-self.b
        return Problem.euclidianNorm(X)<10**8

    def euclidianNorm(X):
        return math.sqrt(sum(map(lambda x: abs(x)**2, X)))

    def compareSolution(self, x_lu):
        A_init = self._computeA_init() 
        x_lib = np.linalg.solve(A_init, self.b)

        return (
            Problem.euclidianNorm(x_lu-x_lib),
            Problem.euclidianNorm(x_lu - np.matmul(np.linalg.inv(A_init), self.b))
        )

    def solve(self):
        sol = dict()

        sol["A"] = self.A.tolist()
        sol["B"] = self.b.tolist()
        sol["epsilon"] = self.epsilon

        sol["canLU"] = self.canComputeLU()

        if sol["canLU"]:
            self.computeLU()
            sol["det"] = "%.15f" % self.computeDeterminant()
            sol["LU"] = self.A.tolist()
            sol["x_lu"] = self.computeX().tolist()
            sol["validSol"] = self.checkSolution(sol["x_lu"])
            sol["x_lib"] = np.linalg.solve(sol["A"], self.b).tolist()
            sol["comp1"] = "%.15f" % Problem.euclidianNorm(np.subtract(sol["x_lu"], sol["x_lib"]))
            sol["comp2"] = "%.15f" % Problem.euclidianNorm(np.subtract(sol["x_lu"], np.matmul(np.linalg.inv(sol["A"]), self.b)))

        return sol


    def display(self):
        print(self.A)
        print(self.b)
        print(self.epsilon)

def init(N, t):
    problem = Problem(N, t)
    print('A init')
    print(problem.A)
    problem.computeLU()
    print('LU')
    print(problem.A)
    solution = problem.computeX()
    print('solution')
    print(solution)
    if problem.checkSolution(solution):
        print('Solution is ok')
        problem.compareSolution(solution)
    else:
        print('Solution is wrong')

if __name__ == "__main__":
    init(100, 8)
