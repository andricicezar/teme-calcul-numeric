import os
import time

from werkzeug.datastructures import ImmutableMultiDict

from tema5 import Problem
from flask_cors import CORS
from utils.utils import settings_init
from flask import Flask, request, redirect, render_template, jsonify

app = Flask(
    __name__,
    # os.path.abspath(os.path.join(os.curdir, 'public')),
    # static_folder='public',
    # template_folder=os.path.abspath(os.path.join(os.curdir, 'templates'))
)

settings = settings_init()
app.template = dict(
    subtitle='Tema 5 - Bugeag George Tiberiu & Andrici Cezar Constantin',
    post_address=settings['address'] + ':' + settings['port']
)
app.template_folder = os.path.join(app.root_path, settings['paths']['templates'])
app.static_folder = os.path.join(app.root_path, settings['paths']['static'])
CORS(app)


@app.context_processor
def inject_template_rendered():
    return dict(template=app.template or dict(
        name='base.html',
        subtitle='Tema 5 - Bugeag George Tiberiu & Andrici Cezar Constantin',
        post_address=settings['address'] + ':' + settings['port']
    ))


@app.route('/')
@app.route('/index')
def redirect_to_home():
    return redirect('/home')


def parse_values_for_random_square(values):
    values['link'] = ''
    values['rare'] = True if values.get('rare') == 'True' else False
    try:
        values['lines'] = int(values.get('lines') or 500)
    except Exception as e:
        print('Exception detected while trying to access {}.\n{}'.format('lines', str(e)))
        values['lines'] = 500
    def_max = 7 * values['lines']
    values['columns'] = values['lines']
    try:
        values['max_elements'] = int(values.get('max_elements') or def_max)
    except Exception as e:
        print('Exception detected while trying to access {}.\n{}'.format('max_elements', str(e)))
        values['max_elements'] = def_max
    try:
        values['max_power_iter'] = int(values.get('max_power_iter') or 1_000_000)
    except Exception as e:
        print('Exception detected while trying to access {}.\n{}'.format('max_power_iter', str(e)))
        values['max_power_iter'] = 1_000_000
    return values


def parse_values_for_read_square(values):
    values['rare'] = True if values.get('rare') == 'True' else False
    def_link = "https://profs.info.uaic.ro/~ancai/CN/lab/5/m_rar_sim_2019_500.txt"
    values['link'] = values.get('link') or def_link if isinstance(values.get('link'), str) else values.get('link') or []
    try:
        values['max_power_iter'] = int(values.get('max_power_iter') or 1_000_000)
    except Exception as e:
        print('Exception detected:\n{}'.format(str(e)))
        values['max_power_iter'] = 1_000_000
    return values


def parse_values_for_random_non_square(values):
    values['link'] = ''
    try:
        values['lines'] = int(values['lines'] or 7)
    except Exception as e:
        print('Exception detected:\n{}'.format(str(e)))
        values['lines'] = 7
    try:
        values['columns'] = int(values.get('columns') or 5)
    except Exception as e:
        print('Exception detected:\n{}'.format(str(e)))
        values['columns'] = 5
    if values['lines'] < values['columns']:
        temp = values['columns']
        values['columns'] = values['lines']
        values['lines'] = temp
    def_max = values['lines'] * values['columns']
    try:
        values['max_elements'] = int(values.get('max_elements')) if values.get('max_elements') or 0 != 0 else def_max
        # values['max_elements'] = int(values.get('max_elements') or def_max)
    except Exception as e:
        print('Exception detected:\n{}'.format(str(e)))
        values['max_elements'] = def_max
    return values


def parse_values_for_read_non_square(values):
    values['link'] = ''
    try:
        values['lines'] = int(values.get('lines') or 7)
    except Exception as e:
        print('Exception detected:\n{}'.format(str(e)))
        values['lines'] = 7
    try:
        values['columns'] = int(values.get('columns') or 5)
    except Exception as e:
        print('Exception detected:\n{}'.format(str(e)))
        values['columns'] = 5
    def_max = values['lines'] * values['columns']
    try:
        values['max_elements'] = int(values.get('max_elements') or def_max)
    except Exception as e:
        print('Exception detected:\n{}'.format(str(e)))
        values['max_elements'] = def_max
    return values


def process_values(values):
    if 'type' not in values:
        raise ValueError('Problem type not specified!')
    try:
        values = ImmutableMultiDict.to_dict(values, flat=False)
        for elm in values:
            if len(values[elm]) == 1:
                values[elm] = values[elm][0]
    except Exception as e:
        print(e)
        raise ValueError('Values type is not supported!')
    if values.get('type') == 'RND_SQUARE':
        return parse_values_for_random_square(values)
    elif values['type'] == 'READ_SQUARE':
        return parse_values_for_read_square(values)
    elif values['type'] == 'RND_NON_SQUARE':
        return parse_values_for_random_non_square(values)
    elif values['type'] == 'READ_NON_SQUARE':
        return parse_values_for_read_non_square(values)
    else:
        raise ValueError('This is not a supported problem type!')


@app.route('/problema1/solution', methods=['POST'])
def symmetric_matrices_solution():
    print("Request for symmetric matrices solution has been detected:")
    start_time = time.time()
    print("Request started at: %s" % time.strftime('%Y-%m-%d %H:%M:%S %p', time.localtime(start_time)))
    try:
        values = process_values(request.form)
        if values['type'] == 'RND_SQUARE':
            the_problem = Problem(**dict(
                problem_type=values['type'],
                link=values['link'],
                dimension=values['lines'],
                max_elements=values['max_elements'],
                max_power_iter=values['max_power_iter'],
                check_if_rare=values['rare']
            ))
        elif values['type'] == 'READ_SQUARE':
            the_problem = Problem(**dict(
                problem_type=values['type'],
                link=values['link'],
                max_power_iter=values['max_power_iter'],
                check_if_rare=values['rare']
            ))
        else:
            the_problem = None
        if the_problem:
            result = the_problem.solution
            result = jsonify(result), 201
        else:
            result = dict(error='The problem was not initiated correctly!')
            result = jsonify(result), 400
        # result["time"] = "%.15f sec(s)" % (time.time() - start_time)
        print("The total estimated time for this request is: %.15f sec(s)" % (time.time() - start_time))
        return result
    except ValueError as e:
        print("The total estimated time for this request is: %.15f sec(s)" % (time.time() - start_time))
        return jsonify(dict(error=str(e))), 400
    except Exception as e:
        print("The total estimated time for this request is: %.15f sec(s)" % (time.time() - start_time))
        return jsonify(dict(error=str(e))), 400


@app.route('/problema2/solution', methods=['POST'])
def classic_matrices_solution():
    print("Request for classic matrices solution has been detected:")
    start_time = time.time()
    print("Start request at: %s" % time.strftime('%Y-%m-%d %H:%M:%S %p', time.localtime(start_time)))
    try:
        values = process_values(request.form)
        if values['type'] == 'RND_NON_SQUARE':
            the_problem = Problem(**dict(
                problem_type=values['type'],
                lines=values['lines'],
                columns=values['columns'],
                max_elements=values['max_elements'],
                link=values['link']
            ))
        elif values['type'] == 'READ_NON_SQUARE':
            the_problem = Problem(**dict(
                problem_type=values['type'],
                lines=values['lines'],
                columns=values['columns'],
                max_elements=values['max_elements'],
                link=values['link']
            ))
        else:
            the_problem = None
        # result["time"] = "%.15f sec(s)" % (time.time() - start_time)
        print("The total estimated time for this request is: %.15f sec(s)" % (time.time() - start_time))
        if the_problem:
            result = jsonify(the_problem.solution), 201
        else:
            result = jsonify(dict(error='The problem was not initiated correctly!')), 400
        print("The total estimated time for this request is: %.15f sec(s)" % (time.time() - start_time))
        # print('The final result is:', the_problem.solution)
        return result
    except ValueError as e:
        print("The total estimated time for this request is: %.15f sec(s)" % (time.time() - start_time))
        return jsonify(dict(error=str(e))), 400


@app.route('/problema1', methods=['GET'])
def matrici_simetrice():
    app.template['name'] = 'problema1.html'
    return render_template(app.template['name'])


@app.route('/problema2', methods=['GET'])
def matrici_clasice():
    app.template['name'] = 'problema2.html'
    return render_template(app.template['name'])


@app.route('/home')
def home_page():
    app.template['name'] = 'index.html'
    return render_template(app.template['name'])


def clean_up():
    dir_files = os.listdir(app.template_folder)
    allowed_templates = [
        'base.html',
        'error.html',
        'index.html',
        'problema1.html'
    ]
    try:
        [
            os.unlink(os.path.abspath(os.path.join(app.template_folder, file)))
            for file in dir_files if file not in allowed_templates
        ]
        return
    except Exception as e:
        print(e)
        pass


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('-a', '--host', type=str, help='Address(IP/DNS) at which the node will be found')
    parser.add_argument('-p', '--port', type=int, help='Port on which the node will listen')
    args = parser.parse_args()
    host = args.host or settings['address']
    port = args.port or settings['port']
    app.run(host=host, port=port, debug=True, threaded=True)
