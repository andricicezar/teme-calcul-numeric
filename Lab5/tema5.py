import re
import json
import time
import math
import numpy
import urllib
from urllib import request


def epsilon_equal(a, b, epsilon):
    return abs(a - b) < (epsilon or (10 ** -9))


def euclid_norm(vec_x):
    return math.sqrt(sum(map(lambda elm: abs(elm) ** 2, vec_x)))


class RareSquareMatrix:
    def __init__(self, dimension, check_if_rare: bool = True):
        self.dimension = dimension
        self.check_if_rare = check_if_rare
        self.data_struct = [[]] * dimension
        self.diag = [float(0)] * dimension
        self.elem_nr = len(self.diag)

    @property
    def epsilon(self):
        return 10 ** -9

    def insert(self, line, column, value):
        if value == 0:
            raise Exception("Zeros are not accepted")

        if line == column:
            self.diag[line] = value
            return

        if len(self.data_struct[line]) == 0:
            self.data_struct[line] = [(column, value)]
            self.elem_nr += 1
            return

        if self.check_if_rare and len(self.data_struct[line]) == 12:
            raise Exception("Too many elements on line " + str(line))

        for elem_idx in range(0, len(self.data_struct[line])):
            if self.data_struct[line][elem_idx][0] == column:
                temp = self.data_struct[line].pop(elem_idx)[1]
                # noinspection PyTypeChecker
                self.data_struct[line].insert(elem_idx, (column, temp + value))
                self.elem_nr += 1
                return
            elif column < self.data_struct[line][elem_idx][0]:
                # noinspection PyTypeChecker
                self.data_struct[line].insert(elem_idx, (column, value))
                self.elem_nr += 1
                return
            else:
                # noinspection PyTypeChecker
                self.data_struct[line].append((column, value))
                self.elem_nr += 1
                return

    def is_equal(self, matrix_b):
        matrix_a = self.data_struct
        matrix_b = matrix_b.matrix.data_struct

        for line in range(0, self.dimension):
            matrix_a_line_len = len(matrix_a[line])

            if matrix_a_line_len != len(matrix_b[line]):
                raise Exception("Matrixes are not equal")

            for elem_idx in range(0, matrix_a_line_len):
                if matrix_a[line][elem_idx][0] != matrix_b[line][elem_idx][0]:
                    return False
                if not epsilon_equal(matrix_a[line][elem_idx][1], matrix_b[line][elem_idx][1], self.epsilon):
                    return False
        return True


class SymSquareRareMatrix(RareSquareMatrix):
    def __init__(self, link=None, dimension=None, check_if_rare=False):
        self.symmetry = None
        if link:
            print("Loading " + link)
            response = urllib.request.urlopen(link).readlines()
            state = 0
            for raw_line in response:
                a_line = raw_line.decode("utf-8").strip()
                if len(a_line) <= 0:
                    continue
                if state == 0:
                    dimension = int(a_line)
                    super(SymSquareRareMatrix, self).__init__(dimension, check_if_rare=check_if_rare)
                    state = 1
                elif state == 1:
                    value, line, column = map(lambda x: x.strip(), a_line.split(','))
                    self.insert(int(line), int(column), float(value))
            self.max_elem = dimension**2
        else:
            super(SymSquareRareMatrix, self).__init__(dimension, check_if_rare=check_if_rare)

    def is_symmetric(self):
        for line_idx in range(self.dimension):
            for elm in range(len(self.data_struct[line_idx])):
                column = self.data_struct[line_idx][elm][0]
                value1 = self.data_struct[line_idx][elm][1]
                value2 = list(filter(lambda x: x[0] == line_idx, self.data_struct[column]))[0][1]
                if not epsilon_equal(value1, value2, self.epsilon):
                    self.symmetry = {
                        'status': False,
                        'line': line_idx,
                        'column': column,
                        'value[{line}][{column}]': value1,
                        'value[{column}][{line}]': value2,
                    }
                    return {
                        'result': False,
                        'text': "For line {} and column {} value is {}, "
                                "but for line {} and column {} value is {}!".format(
                                    line_idx, column, value1, column, line_idx, value2
                                ),
                        'line': line_idx,
                        'column': column,
                        "value1": value1,
                        "value2": value2
                    }
        self.symmetry = {
            'status': True,
            'all': 'ok',
            'timestamp': time.time()
        }
        return {
            'result': True,
            'text': 'The matrix is symmetric'
        }

    def apply_power_method(self, max_iterations: int = 1000000):
        if not max_iterations or not(isinstance(max_iterations, int)) or max_iterations > 1000000:
            max_iterations = 1000000
        if not self.symmetry:
            print('Verifying the symmetry of the matrix first')
            verify = self.is_symmetric()
            if not verify['result']:
                print(verify['text'])
                return
        iter_count = 0
        rand_vector = self.generate_a_random_vector()
        initial_vector = self.generate_matrix_initial_vector(rand_vector)
        product_vector = self.multiply_vector_matrix(initial_vector)
        lambda_value = self.compute_lambda_value(product_vector, initial_vector)
        condition_norm = self.compute_condition_norm(product_vector, initial_vector, lambda_value)

        result_dict = {
            'result': False,
            'iteration': iter_count,
            'lambda': lambda_value,
            'initial_vector': initial_vector
        }
        # print('Verifying power method: ', result_dict)
        iter_count += 1
        while condition_norm > self.dimension * self.epsilon and iter_count <= max_iterations:
            initial_vector = self.generate_matrix_initial_vector(product_vector)
            product_vector = self.multiply_vector_matrix(initial_vector)
            lambda_value = self.compute_lambda_value(product_vector, initial_vector)
            condition_norm = self.compute_condition_norm(product_vector, initial_vector, lambda_value)
            result_dict = {
                'result': False,
                'iteration': iter_count,
                'lambda': lambda_value,
                'initial_vector': initial_vector
            }
            # print('Verifying power method: ', result_dict)
            iter_count += 1

        if iter_count > max_iterations:
            return False, result_dict
        else:
            result_dict['result'] = True
            return True, result_dict

    def generate_a_random_vector(self):
        return list(numpy.random.randint(1, 10, self.dimension))

    def generate_matrix_initial_vector(self, rand_vector):
        if len(rand_vector) != self.dimension:
            raise ValueError('Improper random vector length!')
        vector_norm = self.get_vector_norm(rand_vector)
        return [1 / vector_norm * elem for elem in rand_vector]

    def get_vector_norm(self, rand_vector):
        if len(rand_vector) != self.dimension:
            raise ValueError('Improper random vector length!')
        return math.sqrt(sum(elem**2 for elem in rand_vector))

    def multiply_vector_matrix(self, initial_vector):
        if self.dimension != len(initial_vector):
            raise ValueError('Matrix dimension and vector dimension are not the same')
        vector = [0] * self.dimension
        for la in range(self.dimension):
            vector[la] = self.diag[la] * initial_vector[la]
            for a in self.data_struct[la]:
                vector[la] += a[1] * initial_vector[a[0]]
        return vector

    def compute_lambda_value(self, product_vector, initial_vector):
        if len(initial_vector) != len(product_vector):
            raise ValueError('Length of both vectors should be the same!')
        if len(initial_vector) != self.dimension:
            raise ValueError('Length of both vectors should be the same with the matrix dimension!')
        return sum(initial_vector[idx] * product_vector[idx] for idx in range(self.dimension))

    def compute_condition_norm(self, product_vector, initial_vector, lambda_value):
        if len(initial_vector) != len(product_vector):
            raise ValueError('Length of both vectors should be the same!')
        if len(initial_vector) != self.dimension:
            raise ValueError('Length of both vectors should be the same with the matrix dimension!')
        return self.get_vector_norm(
            [product_vector[idx] - (lambda_value * initial_vector[idx]) for idx in range(self.dimension)]
        )


class RndSymSquareRareMatrix(SymSquareRareMatrix):
    def __init__(self, dimension, max_elem: int = None, check_if_rare: bool = False):
        if max_elem:
            if check_if_rare and max_elem > dimension * 11:
                raise ValueError(
                    'Cannot have more than {} total elements! (11 elements max per line)'.format(11 * dimension)
                )
            if max_elem > dimension * dimension:
                raise ValueError(
                    'A square matrix of dimension {} can only have a max of {}!'.format(dimension, dimension**2)
                )
        self.max_elem = max_elem or (7 * dimension if check_if_rare else dimension * dimension)
        super(RndSymSquareRareMatrix, self).__init__(dimension=dimension, check_if_rare=check_if_rare)
        maximum = ((self.dimension % 3 + 1) * self.dimension) % 17
        maximum *= self.dimension ** ((self.dimension % 5) + (self.dimension % 3)) % 4093496
        maximum = (maximum * maximum) ** (self.dimension % 23) if maximum < self.dimension * self.dimension else maximum
        if maximum > 2 ** 12:
            maximum = 2 ** 12
        # print(maximum)
        for diag_idx in range(0, self.dimension):
            value = numpy.random.randint(1, int(maximum))
            self.insert(diag_idx, diag_idx, value)

        # already_chosen = []
        choices = dict()
        for line_nr in range(dimension):
            choices[line_nr] = list(filter(lambda x: x != line_nr, range(dimension)))
        while self.elem_nr < self.max_elem:
            completion = self.elem_nr * 100 // (self.max_elem - 1)
            print(
                "\r%d %% completed of matrix generation. %d numbers left." % (completion, self.max_elem - self.elem_nr),
                end='',
                flush=True
            )
            line = numpy.random.choice(list(choices.keys()))
            if len(choices[line]) == 0:
                choices.pop(line)
                continue
            if check_if_rare:
                if len(self.data_struct[line]) < 12:
                    column = numpy.random.choice(choices[line])
                    if len(self.data_struct[column]) < 12:
                        value = numpy.random.randint(1, maximum)
                        self.insert(line, column, value)
                        choices[line].remove(column)
                        self.insert(column, line, value)
                        choices[column].remove(line)
                else:
                    choices.pop(line)
            else:
                column = numpy.random.choice(choices[line])
                value = numpy.random.randint(1, maximum)
                self.insert(line, column, value)
                choices[line].remove(column)
                self.insert(column, line, value)
                choices[column].remove(line)
        print("\rMatrix generation completed.")
        # print(f"The total number of elements of this matrix is: {self.elements_number}")


class RndNonSquareMatrix(object):
    def __init__(self, lines: int, columns: int, max_elements: int = None):
        if not isinstance(lines, int):
            raise ValueError('Lines should be int')
        if not isinstance(columns, int):
            raise ValueError('Columns should be int')
        if not isinstance(max_elements, int):
            if not max_elements:
                max_elements = lines * columns
            else:
                raise ValueError('Max_elements should be int')
        if max_elements > lines * columns:
            raise ValueError('For {} lines and {} columns, maximum elements possible is {}'.format(
                lines, columns, lines * columns
            ))
        self.max_elements = max_elements
        self.lines = lines
        self.columns = columns
        self.epsilon = 10 ** -9
        self.data_structure = []
        self.elem_nr = 0
        self.rank = None
        self.eigen_values = None
        self.v_s = None
        self.u_t = None
        self.a_i = None
        self.x_i = None
        for i in range(lines):
            self.data_structure.append([])
            for j in range(columns):
                if self.elem_nr < max_elements:
                    self.data_structure[i].append(numpy.random.randint(1, 11))
                    self.elem_nr += 1
                else:
                    self.data_structure[i].append(0)

    @property
    def matrix_as_dict(self):
        return dict({line: self.data_structure[line] for line in range(len(self.data_structure))})

    def get_random_vector(self):
        return [int(elm) for elm in numpy.random.randint(1, 11, size=self.lines)]

    def compute_pseudo_reverse_matrix(self, vector):
        u, s, vh = numpy.linalg.svd(self.data_structure, full_matrices=True)
        positive_eigen_values_list = list(filter(lambda x: x > 0, s))
        if not self.rank:
            self.compute_rang(positive_eigen_values_list)
        s_i_matrix = self.generate_s_i()
        for idx in range(self.columns):
            if idx < self.rank[0]:
                s_i_matrix[idx].insert(idx, 1 / positive_eigen_values_list[idx])
                s_i_matrix[idx].pop(-1)
        self.v_s = numpy.dot(vh, s_i_matrix)
        self.u_t = numpy.transpose(u)
        self.a_i = numpy.dot(self.v_s, self.u_t)
        self.x_i = self.multiply_vector_identity_matrix(vector)

        return self.x_i

    def compute_rang(self, eigen_values_list):
        self.rank = len([elm for elm in eigen_values_list if elm > self.epsilon]), eigen_values_list
        self.eigen_values = eigen_values_list

    def generate_s_i(self):
        return [[0 for j in range(self.lines)] for i in range(self.columns)]

    def multiply_vector_identity_matrix(self, vector, other_a_i=None):
        if other_a_i:
            lines, cols = len(other_a_i), len(other_a_i[0])
            return [
                sum(other_a_i[line_idx][col_idx] * vector[col_idx] for col_idx in range(cols))
                for line_idx in range(lines)
            ]
        else:
            lines, cols = len(self.a_i), len(self.a_i[0])
            self.x_i = [
                sum(self.a_i[line_idx][col_idx] * vector[col_idx] for col_idx in range(cols))
                for line_idx in range(lines)
            ]
            return self.x_i

    def compute_difference_norm(self, vector):
        new_x_i = self.multiply_vector_identity_matrix(self.x_i, other_a_i=self.data_structure)
        return self.get_vector_norm([vector[line_idx] - new_x_i[line_idx] for line_idx in range(self.lines)])

    def get_vector_norm(self, rand_vector):
        if len(rand_vector) != self.lines:
            raise ValueError('Improper random vector length!')
        return math.sqrt(sum(elem**2 for elem in rand_vector))

    def compute_conditioning_number(self):
        return max(self.rank[1]) / min([elm for elm in self.rank[1] if elm > self.epsilon])


class Problem(object):
    def __init__(self, *args, **kwargs):
        self.__solution_dict__ = None
        if args:
            print('Args were passed:\n {}'.format(args))
        if not kwargs:
            raise ValueError('No arguments were passed to define the problem!')
        else:
            required = ['problem_type', 'link']
            if not all(req in kwargs for req in required):
                raise ValueError('Not all needed arguments were passed!')
            self.problem_type = kwargs.get('problem_type')
            if self.problem_type == 'RND_SQUARE':
                try:
                    dimension = int(kwargs.get('dimension') or kwargs.get('lines') or kwargs.get('columns') or 500)
                except Exception as e:
                    print('Exception detected while trying to access {}.\n{}'.format('dimension', str(e)))
                    dimension = 500
                try:
                    max_elements = int(kwargs.get('max_elements') or (15 * (dimension ** 2) / 100))
                except Exception as e:
                    print("Exception detected:\n {}".format(e))
                    max_elements = 15 * dimension ** 2 / 100
                rare = kwargs.get('check_if_rare') or False
                rnd_matrix = RndSymSquareRareMatrix(dimension=dimension, max_elem=max_elements, check_if_rare=rare)
                start_time = time.time()
                try:
                    max_power_iterations = int(kwargs.get('max_power_iter')) or 1_000_000
                except Exception as e:
                    print("Exception detected:\n {}".format(e))
                    max_power_iterations = 1_000_000
                self.__solution_dict__ = {
                    'total_elements': rnd_matrix.elem_nr,
                    'max_elements': rnd_matrix.max_elem,
                    'symmetry': rnd_matrix.is_symmetric(),
                    'power_method': rnd_matrix.apply_power_method(max_iterations=max_power_iterations)[1]
                }
                self.__solution_dict__.update({'elapsed_time': "%.4f" % (time.time() - start_time)})
            elif self.problem_type == 'READ_SQUARE':
                try:
                    max_power_iterations = int(kwargs.get('max_power_iter') or 1_000_000)
                except Exception as e:
                    print("Exception detected:\n {}".format(e))
                    max_power_iterations = 1_000_000
                link = kwargs.get('link')
                default_links = [
                    "https://profs.info.uaic.ro/~ancai/CN/lab/5/m_rar_sim_2019_500.txt",
                    "https://profs.info.uaic.ro/~ancai/CN/lab/5/m_rar_sim_2019_1000.txt",
                    "https://profs.info.uaic.ro/~ancai/CN/lab/5/m_rar_sim_2019_1500.txt",
                    "https://profs.info.uaic.ro/~ancai/CN/lab/5/m_rar_sim_2019_2019.txt",
                ]
                link_regex = re.compile(
                    r'^(?:http|ftp)s?://'  # http:// or https://
                    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain
                    r'localhost|'  # localhost
                    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ip
                    r'(?::\d+)?'  # optional port
                    r'(?:/?|[/?]\S+)$', re.IGNORECASE)
                rare = kwargs.get('check_if_rare') or False
                if isinstance(link, list):
                    links = [
                        validated
                        for validated in link
                        if validated in default_links or
                        link_regex.match(validated) is not None
                    ]
                    if not links or len(links) == 0:
                        print('Link values were either not supported or malformed. Initiating default links.')
                        links = default_links[:-1]
                    fin_dict = {f'{nr}': {} for nr in range(len(links))}
                    for idx in range(len(links)):
                        the_matrix = SymSquareRareMatrix(link=links[idx], check_if_rare=rare)
                        start_time = time.time()
                        fin_dict.update({
                            f'{idx}': {
                                'link': links[idx],
                                'total_elements': the_matrix.elem_nr,
                                'max_elements': the_matrix.max_elem,
                                'symmetry': the_matrix.is_symmetric(),
                                'power_method': the_matrix.apply_power_method(max_iterations=max_power_iterations)[1]
                            }
                        })
                        fin_dict.get(idx).update({'elapsed_time': "%.4f" % (time.time() - start_time)})
                    self.__solution_dict__ = fin_dict
                elif isinstance(link, str):
                    if link in default_links or link_regex.match(link):
                        the_matrix = SymSquareRareMatrix(link=link, check_if_rare=rare)
                        start_time = time.time()
                        self.__solution_dict__ = {
                            '0': {
                                'link': link,
                                'total_elements': the_matrix.elem_nr,
                                'max_elements': the_matrix.max_elem,
                                'symmetry': the_matrix.is_symmetric(),
                                'power_method': the_matrix.apply_power_method(max_iterations=max_power_iterations)[1]
                            }
                        }
                        self.__solution_dict__.get('0').update({'elapsed_time': "%.4f" % (time.time() - start_time)})
                    else:
                        raise ValueError('Link value is not supported!')
                else:
                    raise ValueError('Link value is not supported!')
            elif self.problem_type == 'RND_NON_SQUARE':
                try:
                    lines = int(kwargs.get('lines')) or 7
                except Exception as e:
                    print("Exception detected:\n {}".format(e))
                    lines = 7
                try:
                    columns = int(kwargs.get('columns')) or 5
                except Exception as e:
                    print("Exception detected:\n {}".format(e))
                    columns = 5
                try:
                    max_elements = int(kwargs.get('max_elements')) or None
                except Exception as e:
                    print("Exception detected:\n {}".format(e))
                    max_elements = None
                the_ns_matrix = RndNonSquareMatrix(lines=lines, columns=columns, max_elements=max_elements)
                start_time = time.time()
                self.__solution_dict__ = dict()
                self.__solution_dict__['max_elements'] = the_ns_matrix.max_elements
                self.__solution_dict__['total_elements'] = the_ns_matrix.lines * the_ns_matrix.columns
                self.__solution_dict__['matrix'] = the_ns_matrix.data_structure
                self.__solution_dict__['matrix_dict'] = the_ns_matrix.matrix_as_dict
                self.__solution_dict__['random_vector'] = the_ns_matrix.get_random_vector()
                self.__solution_dict__['solution'] = the_ns_matrix.compute_pseudo_reverse_matrix(
                    self.__solution_dict__['random_vector']
                )
                self.__solution_dict__['norm'] = the_ns_matrix.compute_difference_norm(
                    self.__solution_dict__['random_vector']
                )
                self.__solution_dict__['rank'] = the_ns_matrix.rank[0]
                self.__solution_dict__['eigen_values'] = the_ns_matrix.eigen_values or the_ns_matrix.rank[1]
                self.__solution_dict__['conditioning_number'] = the_ns_matrix.compute_conditioning_number()
                self.__solution_dict__['elapsed_time'] = "%.4f" % (time.time() - start_time)
            elif self.problem_type == 'READ_NON_SQUARE':
                print('Don\'t think we actually need this one lol.')
                raise ValueError('Problem of this type is not implemented yet!')
            else:
                raise ValueError('Problem type argument non supported!')

    @property
    def solution(self):
        if not self.__solution_dict__:
            raise ValueError('Problem must be first defined!')
        return self.__solution_dict__


if __name__ == "__main__":
    matrix = RndSymSquareRareMatrix(dimension=500, max_elem=5*(500*500)//100, check_if_rare=False)
    print("Matrix symmetry: {}, Total elements: {}".format(matrix.is_symmetric(), matrix.elem_nr))
    power_method = matrix.apply_power_method()
    # print(power_method[0])
    print(power_method[1])
    the_links = [
        # "https://profs.info.uaic.ro/~ancai/CN/lab/5/m_rar_sim_2019_500.txt",
        # "https://profs.info.uaic.ro/~ancai/CN/lab/5/m_rar_sim_2019_1000.txt",
        # "https://profs.info.uaic.ro/~ancai/CN/lab/5/m_rar_sim_2019_1500.txt",
        # "https://profs.info.uaic.ro/~ancai/CN/lab/5/m_rar_sim_2019_2019.txt",
    ]
    for file_link in the_links:
        matrix = SymSquareRareMatrix(link=file_link)
        print("Matrix symmetry: {}, Total elements: {}".format(matrix.is_symmetric(), matrix.elem_nr))
        power_method = matrix.apply_power_method()
        # print(power_method[0])
        print(power_method[1])
    non_square_m_test = RndNonSquareMatrix(17, 15)
    test_vec = non_square_m_test.get_random_vector()
    # This is hardcoded just for test!!!
    # non_square_m_test.data_structure = [
    #     [7, 7, 4, 3, 2],
    #     [10, 10, 1, 8, 1],
    #     [7, 7, 4, 3, 6],
    #     [5, 5, 4, 1, 3],
    #     [9, 9, 2, 9, 8],
    #     [8, 8, 3, 8, 3],
    #     [1, 1, 4, 6, 7]
    # ]
    # test_vec = [6, 9, 1, 8, 1, 3, 8]

    sol = non_square_m_test.compute_pseudo_reverse_matrix(test_vec)
    final_res = {
        'matrix': non_square_m_test.matrix_as_dict,
        'vector_rand': test_vec,
        'solution': sol,
        'norm': non_square_m_test.compute_difference_norm(test_vec),
        'rank': non_square_m_test.rank[0],
        'eigen_values': non_square_m_test.rank[1],
        'conditioning_nr': non_square_m_test.compute_conditioning_number()
    }
    # print(final_res)
    # print(final_res)
    print(json.dumps(final_res, indent=4))
