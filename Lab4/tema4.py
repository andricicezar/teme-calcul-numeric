import math
import urllib
from urllib import request


class RareMatrix:
    def __init__(self, N, checkIfRare=True):
        self.N = N
        self.checkIfRare = checkIfRare
        self.ds = [[]] * N
        self.diag = [0] * N

    def insert(self, i, j, v):
        if v == 0:
            raise Exception("Zeros are not accepted")

        if i == j:
            self.diag[i] = v
            return

        if len(self.ds[i]) == 0:
            self.ds[i] = [(j, v)]
            return 0

        if len(self.ds[i]) == 12 and self.checkIfRare:
            raise Exception("Too many elements on line " + str(i))

        for k in range(0, len(self.ds[i])):
            if self.ds[i][k][0] == j:
                self.ds[i][k] = (j, self.ds[i][k][1] + v)
                return
            elif j < self.ds[i][k][0]:
                self.ds[i] = self.ds[i][:k] + [(j, v)] + self.ds[i][k:]
                return k

        self.ds[i].append((j, v))

    def equals(self, B):
        A = self.ds
        B = B.matrix.ds

        for l in range(0, self.N):
            a = len(A[l])

            if a != len(B[l]):
                raise Exception("Matrixes are not equal")

            for i in range(0, a):
                if A[l][i][0] != B[l][i][0] or not equals(A[l][i][1], B[l][i][1]):
                    return False

        return True


def equals(a, b):
    return abs(a - b) < (10 ** -8)


def euclidianNorm(X):
    return math.sqrt(sum(map(lambda x: abs(x) ** 2, X)))


class CustomMatrix:
    def __init__(self, link, checkIfRare=True):
        print("Loading " + link)
        response = urllib.request.urlopen(link).readlines()
        self.b = []
        state = 0
        for rawLine in response:
            line = rawLine.decode('utf-8').strip()

            if state == 0:
                self.matrix = RareMatrix(int(line), checkIfRare)
                state = 1
            elif state == 1:
                # empty line
                state = 2
            elif state == 2 and len(line) > 0:
                v, i, j = map(lambda x: x.strip(), line.split(','))
                self.matrix.insert(int(i), int(j), float(v))
            elif state == 2:
                # empty line
                state = 3
            elif state == 3 and len(line) > 0:
                self.b.append(float(line))
            elif state == 3:
                state = 4

    def multiplyWithAColumn(self, column):
        X = [0] * self.matrix.N
        A = self.matrix.ds

        for la in range(0, self.matrix.N):
            X[la] = self.matrix.diag[la] * column[la]
            for a in A[la]:
                if la == 0:
                    print(la, a[0], a[1])
                X[la] += a[1] * column[a[0]]

        return X

    def calculateNorm(self, xsor):
        print('Calculate norm')
        x = self.multiplyWithAColumn(xsor)
        print(x[:10])
        print(self.b[:10])
        for i in range(0, len(self.b)):
            x[i] -= self.b[i]
        # print(x[:10])
        print(euclidianNorm(x))

        return x

    def searchSolution(self, xsor, w, epsilon, dmax, kmax):
        k = 0
        A = self.matrix.ds
        while True:
            d = 0
            k += 1
            for i in range(0, len(xsor)):
                save = xsor[i]
                S = 0

                for el in A[i]:
                    S += el[1] * xsor[el[0]]

                xsor[i] = (1 - w) * save + w / self.matrix.diag[i] * (self.b[i] - S)
                d += abs(save - xsor[i])

            if d < epsilon:
                self.calculateNorm(xsor)
                return [True, w, k, xsor[:10]]

            if k > kmax:
                return [False, w, "Divergence, lower epsilon"]

            if d > dmax:
                return [False, w, "Divergence, change method"]


class Problem:
    def __init__(self, link, epsilon, dmax, kmax):
        self.link = link
        self.epsilon = epsilon
        self.dmax = dmax
        self.kmax = kmax

    def solve(self):
        result = list()
        A = CustomMatrix(self.link)
        result.append(
            A.searchSolution([0] * A.matrix.N, 0.8, self.epsilon, self.dmax, self.kmax)
        )

        result.append(
            A.searchSolution([0] * A.matrix.N, 1, self.epsilon, self.dmax, self.kmax)
        )

        result.append(
            A.searchSolution([0] * A.matrix.N, 1.2, self.epsilon, self.dmax, self.kmax)
        )

        return result


if __name__ == "__main__":
    # A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/4/m_rar_2019_1.txt")
    A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/3/a.txt")
    print(A.searchSolution([0] * A.matrix.N, 0.8, 10 ** -10, 10 ** 10, 1000))
    # print('\n')
    # A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/4/m_rar_2019_2.txt")
    # A.searchSolution([0] * A.matrix.N, 0.8, 10 ** -10, 1000)
    # print('\n')
    # A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/4/m_rar_2019_3.txt")
    # A.searchSolution([0] * A.matrix.N, 0.8, 10 ** -10, 1000)
    # A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/4/m_rar_2019_4.txt")
    # A.searchSolution([0] * A.matrix.N, 0.8, 10 ** -10, 1000)
