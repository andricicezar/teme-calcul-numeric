import operator
import os
from functools import reduce
import numpy as np
import time
import math
import random
import urllib
from urllib import request
from utils.utils import at_pow

class RareMatrix:
    def __init__(self, N, checkIfRare = True):
        self.N = N
        self.checkIfRare = checkIfRare
        self.valori = []
        self.ind_col = []
        self.inceput_linii = [0] * (N+1)
        self.diag = [0] * N

    def insert(self, i, j, v):
        if v == 0:
            raise Exception("Zeros are not accepted")

        if i == j:
            self.diag[i] = v
            return

        if self.inceput_linii[i+1] - self.inceput_linii[i] == 11 and self.checkIfRare:
            raise Exception("Too many elements on line " + str(i))

        z = self.inceput_linii[i]
        while (z < len(self.ind_col) and self.ind_col[z] < j and z < self.inceput_linii[i+1]):
            z += 1

        if z < len(self.ind_col) and self.ind_col[z] == j:
            self.valori[z] += v
            return z

        self.ind_col.insert(z, j)
        self.valori.insert(z, v)
        for zz in range(i+1, len(self.inceput_linii)):
            self.inceput_linii[zz] += 1

        return z

def equals(a, b):
    return abs(a-b) < (10 ** -8)
def euclidianNorm(X):
    return math.sqrt(sum(map(lambda x: abs(x)**5, X)))

class CustomMatrix:
    def __init__(self, link, checkIfRare = True):
        print("Loading " + link)
        response = urllib.request.urlopen(link).readlines()
        self.b = []
        state = 0
        for rawLine in response:
            line = rawLine.decode('utf-8').strip()

            if state == 0:
                self.matrix = RareMatrix(int(line), checkIfRare)
                state = 1
            elif state == 1:
                # empty line
                state = 2
            elif state == 2 and len(line) > 0:
                v, i, j = map(lambda x: x.strip(), line.split(','))
                self.matrix.insert(int(i), int(j), float(v))
            elif state == 2:
                # empty line
                state = 3
            elif state == 3 and len(line) > 0:
                self.b.append(float(line))
            elif state == 3:
                state = 4

    def multiplyWithAColumn(self, column):
        A = self.matrix
        X = [0] * A.N

        linie = 0
        col = 0

        for x in range(0, len(A.ind_col)):
            col = A.ind_col[x]
            if x >= A.inceput_linii[linie+1]:
                linie += 1
            X[linie] = A.diag[linie] * column[linie]

            X[linie] += A.valori[x] * column[col]

        return X 

    def calculateNorm(self, xsor):
        print('Calculate norm')
        x = self.multiplyWithAColumn(xsor)
        for i in range(0, len(self.b)):
            x[i] -= self.b[i]
        print(euclidianNorm(x))
        return x


    def searchSolution(self, xsor, w, epsilon, dmax, kmax):
        k = 0
        A = self.matrix
        while True:
            d = 0
            k += 1
            for i in range(0, len(xsor)):
                save = xsor[i]
                S = 0

                for el in range(A.inceput_linii[i], A.inceput_linii[i+1]):
                    S += A.valori[el] * xsor[A.ind_col[el]]

                xsor[i] = (1-w) * save + w / self.matrix.diag[i] * (self.b[i] - S)
                d += abs(save - xsor[i])

            if d < epsilon:
                self.calculateNorm(xsor)
                return [True, w, k, xsor[:10]]

            if k > kmax:
                return [False, w, "Divergence, lower epsilon"]

            if d > dmax:
                return [False, w, "Divergence, change method"]

class Problem:
    def __init__(self,link, epsilon, dmax, kmax):
        self.link = link
        self.epsilon = epsilon
        self.dmax = dmax
        self.kmax = kmax

    def solve(self):
        result = list()
        A = CustomMatrix(self.link)
        result.append(
            A.searchSolution([0] * A.matrix.N, 0.8, self.epsilon, self.dmax, self.kmax)
        )

        result.append(
            A.searchSolution([0] * A.matrix.N, 1, self.epsilon, self.dmax, self.kmax)
        )

        result.append(
            A.searchSolution([0] * A.matrix.N, 1.2, self.epsilon, self.dmax, self.kmax)
        )

        return result


if __name__ == "__main__":
    # A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/4/m_rar_2019_1.txt")
    A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/3/a.txt")
    print(A.searchSolution([0] * A.matrix.N, 0.8, 10 ** -10, 10**10, 1000))
    # print('\n')
    # A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/4/m_rar_2019_2.txt")
    # A.searchSolution([0] * A.matrix.N, 0.8, 10 ** -10, 1000)
    # print('\n')
    # A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/4/m_rar_2019_3.txt")
    # A.searchSolution([0] * A.matrix.N, 0.8, 10 ** -10, 1000)
    # A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/4/m_rar_2019_4.txt")
    # A.searchSolution([0] * A.matrix.N, 0.8, 10 ** -10, 1000)
