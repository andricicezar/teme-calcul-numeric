import os
import json
import re
import time
import uuid
import urllib
from urllib import request
from io import TextIOWrapper
from collections.abc import Iterable
from datetime import date, datetime
from werkzeug.http import http_date


def at_pow(nr, power):
    if power < 0:
        return 0
    if power == 0:
        return 1
    if power == 1:
        return nr
    if power == 2:
        return nr * nr
    if power > 2:
        if power % 2 == 0:
            return at_pow(at_pow(nr, power // 2), 2)
        if power % 2 == 1:
            return at_pow(at_pow(nr, power // 2), 2) * nr


def settings_init():
    return dict(
        paths=dict(
            templates='templates',
            static='public',
            poly=os.path.join('public', os.path.join('poly', 'polynomial.txt')),
            saves=os.path.join('public', 'saves'),
        ),
        address='127.0.0.1',
        port='27001',
        protocol='http://',
    )


def json_encode(obj):
    try:
        if isinstance(obj, dict):
            return {key: obj[key] for key in obj}
        if isinstance(obj, datetime):
            return http_date(obj.utctimetuple())
        if isinstance(obj, date):
            return http_date(obj.timetuple())
        if isinstance(obj, uuid.UUID):
            return str(obj)
        iterable = list(iter(obj))
    except TypeError as e:
        print(e)
        return json.JSONEncoder.default(json.JSONEncoder(), obj)
    else:
        return {f"{idx+1}": iterable[idx] for idx in range(0, len(iterable))}


def get_save_file_handler(custom_name: str = None, exists: bool = None):
    if custom_name and isinstance(custom_name, str) and exists and os.path.exists(custom_name):
        return open(custom_name, 'w+')
    elif custom_name and exists:
        return open(custom_name, 'w+')
    else:
        if not custom_name:
            custom_name = "save_file"
        custom_name += "_" + time.strftime('%Y-%m-%d_%H-%M-%S_%z') + '.json'
        file_path = os.path.normpath(
            os.path.join(
                os.path.join(
                    os.path.join(
                        os.path.dirname(os.path.dirname(__file__)),
                        'public'
                    ),
                    'saves',
                ),
                custom_name
            )
        )
        return open(file_path, 'w+')


def dump_encoded_object(obj,
                        file_name=None, file_exists: bool = None,
                        indent: int = None, sort_keys: bool = None,
                        return_handler: bool = None, set_filename: bool = None):
    """
    Function that will encode into a JSON like dictionary an iterable object and will dump it in a file.

    :param obj: The iterable object
    :type obj: Iterable
    :param file_name: Can be either the name of the file as a string, or a file handler
    :type file_name: None or [str or TextIOWrapper]
    :param file_exists: Flag if file exists. True if file_name is a handler.
    :type file_exists: None or bool
    :param indent: Indentation size for the object keys. Default none.
    :type indent: None or int
    :param sort_keys: Flag that controls if keys should be sorted.
    :type sort_keys: None or bool
    :param return_handler: Flag that controls if the handler of the save file should be returned
    :type return_handler: bool
    :param set_filename: Flag that controls if the object supports a 'file_name' field. The object should be a dict!
    :type set_filename: bool
    :return: None or the save file handler
    :rtype: None or TextIOWrapper
    """
    if file_exists and isinstance(file_name, TextIOWrapper):
        fh = file_name
        if set_filename and isinstance(obj, dict):
            obj['file_name'] = fh.name
        json.dump(obj, file_name, indent=indent, sort_keys=sort_keys, default=json_encode)
    else:
        fh = get_save_file_handler(custom_name=file_name, exists=file_exists)
        if set_filename and isinstance(obj, dict):
            obj['file_name'] = fh.name
        json.dump(
            obj,
            fh,
            indent=indent,
            sort_keys=sort_keys,
            default=json_encode
        )
    if return_handler:
        return fh
    else:
        try:
            fh.close()
        except TextIOWrapper:
            pass


def read_poly_from_file(file_name, mode=None):
    if mode and mode not in ['r', 'r+']:
        raise ValueError('Improper reading method detected!')
    with open(file_name, mode or 'r') as fd:
        coefficient_list = [eval(coefficient) for coefficient in fd.read().strip('\n').split()]
    return coefficient_list


def read_expected_output_from_file(file_name, mode=None):
    if mode and mode not in ['r', 'r+']:
        raise ValueError('Improper reading method detected!')
    with open(file_name, mode or 'r') as fd:
        expected_output = [eval(output) for output in fd.read().strip('\n').split()]
    return expected_output


def write_poly_to_file(file_name, poly, mode=None):
    if isinstance(poly, list) is False:
        raise ValueError('The polynomial parameter should be a list containing its coefficients!')
    if mode and mode not in ['w', 'w+', 'a', 'a+']:
        raise ValueError('Improper writing method detected!')
    with open(file_name, mode or 'w+') as fd:
        fd.write(' '.join([str(elm) for elm in poly]))
    return True


def is_link(link: str):
    return link.startswith('http')


def read_poly_from_link(link):
    response = urllib.request.urlopen(link)
    coding = response.headers.get_content_charset('default')
    the_list = []
    the_nr = ""
    for elm in response.read().decode(coding):
        if not re.match(r'[\w\-.]', elm):
            if len(the_nr) != 0:
                the_list.append(the_nr)
            the_nr = ""
        else:
            the_nr += f"{elm}"
    coefficient_list = [
        eval(coefficient) for coefficient in the_list
    ]

    return coefficient_list


def read_expected_output_from_link(link):
    response = urllib.request.urlopen(link)
    coding = response.headers.get_content_charset('default')
    the_list = []
    the_nr = ""
    for elm in response.read().decode(coding):
        if not re.match(r'[\w\-.]', elm):
            if len(the_nr) != 0:
                the_list.append(the_nr)
            the_nr = ""
        else:
            the_nr += f"{elm}"
    expected_output_list = [
        eval(output) for output in the_list
    ]

    return expected_output_list


# if __name__ == '__main__':
    # fh = get_save_file_handler('test_save_file')
    # # print(type(fh))
    # the_list = [1, 2, 3, 3, 4, 5, 6, 6, 7, 7, 7, 7, 7, 8, 9, 10]
    # dump_encoded_object(the_list, fh, file_exists=True, indent=4, sort_keys=False)
    # print(
    # for elm in read_poly_from_link('http://127.0.0.1:27000/polinom.txt'):
    #     print(elm)
    # )
