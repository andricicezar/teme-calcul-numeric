import numpy
import random
from Lab6.tema6 import PolySolver
from Lab8.utils.utils import *


class SteffensenPolySolver(PolySolver):
    def __init__(self, function_file, output_file):
        self.output_input = output_file
        self.expected_output = self.read_expected_output_from_source()
        super().__init__(function_file)
        self.epsilon['steffs_high'] = pow(10, -8)
        self.epsilon['steffs_med'] = pow(10, -7)
        self.epsilon['steffs_low'] = pow(10, -6)
        self.epsilon['steffs_lowest'] = pow(10, -5)

    def read_expected_output_from_source(self):
        if is_link(self.output_input) is False:
            return read_expected_output_from_file(self.output_input)
        else:
            return read_expected_output_from_link(self.output_input)

    def approximate_first_derived(self, value, function, h=None, method=None):
        """
        Approximates the second derivate of function 'function' given as a vector of coefficient in value 'value'.
        :param value: number
        :param function: vector of floats
        :param h: number (preferably 10**(-5) or 10**(-6))
        :param method: number (1 or anything else))
        :return result: float
        """
        if h is None or not self.epsilon['steffs_lowest'] >= h >= self.epsilon['steffs_low']:
            h = self.epsilon['steffs_lowest']
        if method is None:
            method = 1
        if method == 1:
            result = (3 * self.compute_horn_scheme_result(function, value) - 4 * self.compute_horn_scheme_result(
                function, value - h) +
                      self.compute_horn_scheme_result(function, value - 2 * h)) / (2 * h)
        else:
            result = (-self.compute_horn_scheme_result(function, value + 2 * h) + 8 * self.compute_horn_scheme_result(
                function, value + h) -
                      8 * self.compute_horn_scheme_result(function, value - h) + self.compute_horn_scheme_result(
                    function, value - 2 * h)) \
                     / (12 * h)
        return result

    def approximate_second_derived(self, value, function, h=None):
        """
        Approximates the second derivate of function 'function' given as a vector of coefficient in value 'value'.
        :param value: number
        :param function: vector of floats
        :param h: number (preferably 10**(-5) or 10**(-6))
        :return result: float
        """
        if h is None or not self.epsilon['steffs_lowest'] >= h >= self.epsilon['steffs_low']:
            h = self.epsilon['steffs_lowest']
        result = -1 * self.compute_horn_scheme_result(function, value + 2 * h)
        result += 16 * self.compute_horn_scheme_result(function, value + h)
        result -= 30 * self.compute_horn_scheme_result(function, value)
        result += 16 * self.compute_horn_scheme_result(function, value - h)
        result -= self.compute_horn_scheme_result(function, value - 2 * h)
        result /= 12 * (h ** 2)
        return result

    def compute_delta_x(self, value, function):
        """
        :param value: number
        :param function: vector of floats
        :return delta_x: number
        """
        try:
            delta_x = (
                (self.approximate_first_derived(value, function) ** 2) /
                (
                    self.approximate_first_derived(
                        value + self.approximate_first_derived(value, function),
                        function
                    ) - self.approximate_first_derived(value, function)
                )
            )
        except Exception as e:
            print(f'Exception caught: {e}')
            return pow(10, 9)
        return delta_x

    def steffensen_method(self, max_iterations=5000, interval=10):
        function, expected_output = self.coefficient_list, self.expected_output
        iteration = 1
        x_previous = random.randint(a=-interval, b=interval)
        x_current = x_previous
        if abs(self.approximate_first_derived(x_current, function)) <= self.epsilon['steffs_lowest']:
            return {
                "result": "Solution found",
                "x_current": x_current,
                "diffs": [abs(x_current - output) for output in expected_output],
                "iterations": iteration
            }
        delta_x = self.compute_delta_x(x_previous, function)
        if abs(delta_x) < self.epsilon['steffs_lowest']:
            return {
                "result": "Solution found",
                "x_current": x_current,
                "diffs": [abs(x_current - output) for output in expected_output],
                "iterations": iteration
            }
        if abs(delta_x) > pow(10, 8):
            return {
                "result": "Divergence reached",
                "x_current": None,
                "diffs": None,
                "iterations": iteration
            }
        x_current -= delta_x
        while self.epsilon['steffs_lowest'] <= abs(delta_x) <= self.epsilon['upper'] and iteration <= max_iterations:
            if abs(self.approximate_first_derived(x_current, function)) <= self.epsilon['steffs_lowest']:
                return {
                    "result": "Solution found",
                    "x_current": x_current,
                    "diffs": [abs(x_current - output) for output in expected_output],
                    "iterations": iteration
                }
            delta_x = self.compute_delta_x(x_previous, function)
            x_previous = x_current
            x_current -= delta_x
            iteration += 1
        if iteration > max_iterations:
            return {
                "result": f"Maximum iterations reached ({max_iterations})",
                "x_current": None,
                "diffs": None,
                "iterations": iteration
            }
        if abs(delta_x) < self.epsilon['steffs_lowest']:
            return {
                "result": "Solution found",
                "x_current": x_current,
                "diffs": [abs(x_current - output) for output in expected_output],
                "iterations": iteration
            }
        return {
            "result": "Divergence reached",
            "x_current": None,
            "diffs": None,
            "iterations": iteration
        }

    def run_steff_until_result(self, max_tries=None, max_steff_iteration=None):
        if not max_tries or not isinstance(max_tries, int):
            max_tries = 10000
        if not max_steff_iteration or not isinstance(max_steff_iteration, int):
            max_steff_iteration = 10000
        result = self.steffensen_method(max_iterations=max_steff_iteration)
        tries: int = None
        for iteration in range(max_tries):
            tries = iteration
            if result['result'] == "Solution found":
                result = self.steffensen_method()
                break
            else:
                result = self.steffensen_method(max_iterations=max_steff_iteration)
        return {"steffensen_result": result, "tries": tries}


if __name__ == '__main__':
    print("Minimum of a function")
    steff_poly = SteffensenPolySolver('function1.txt', 'output1.txt')
    print(steff_poly.run_steff_until_result(10000, 10000))
