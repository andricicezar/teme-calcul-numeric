import numpy
from Lab6.utils.utils import *


class PolySolver(object):
    """

    :param file_name: File name or url
    :type file_name: str
    """
    def __init__(self, file_name):
        self.epsilon = {
            'highest': 10 ** -16,
            'high': 10 ** -15,
            'medium': 10 ** -14,
            'normal': 10 ** -13,
            'lower': 10 ** -12,
            'low': 10 ** -11,
            'lowest': 10 ** -10,
            'upper': 10 ** 8
        }
        self.max_iter = {
            'halley': 10000
        }
        self.poly_file_input = file_name
        self.coefficient_list = self.read_poly_from_source()
        self.accuracy_range = {10 ** elm for elm in range(-16, 8)}
        self.solution = dict()
        self.solution['horn'] = dict()
        self.solution['gcd'] = dict()
        self.solution['multiplicity'] = dict()
        self.solution['halley'] = dict()
        self.solution['output']: TextIOWrapper = None
        self.solution['file_name']: str = None
        self.solution['elapsed']: float = 0.00
        self.solution['expiration']: float = 0.00

    @property
    def poly_degree(self):
        return len(self.coefficient_list) - 1

    def solve(self, print_it: bool = None):
        self.solution['elapsed'] = time.time()
        self.solution['horn']['result'] = self.get_horn_scheme_string()
        self.solution['horn']['text'] = f"Poly decomposition based on Horner's scheme is: " \
            f"{self.solution['horn']['result']}. "
        self.coefficient_list = self.read_poly_from_source()
        self.solution['gcd']['result'] = self.compute_gcd()
        self.solution['gcd']['text'] = f"The greatest common divisors are: " \
            f"{', '.join([str(elm) for elm in list(self.solution['gcd']['result'])])};"
        self.solution['multiplicity']['text'] = f"Multiplicity roots greater than 1 cleanup. " \
            f"It is done based on the application of Euclid's C.M.M.D.C algorithm on P(x) and P'(x): " \
            f"Q = CMMDC(P, P`), meaning "

        if len(self.solution['gcd']['result']) > 1:
            self.solution['multiplicity']['result'] = self.compute_euclid(
                self.coefficient_list, self.solution['gcd']['result']
            )[0]
            self.coefficient_list = self.solution['multiplicity']['result']
            self.solution['multiplicity']['text'] += f"{self.get_horn_scheme_string(self.solution['gcd']['result'])}." \
                f" Because it is not a simple constant, multiplicity roots will be cleaned and reach this: " \
                f"{', '.join([str(elm) for elm in self.solution['multiplicity']['result']])}" \
                f", represented in Horner's scheme as: " \
                f"{self.get_horn_scheme_string(self.solution['multiplicity']['result'])}';"
            self.solution['multiplicity']['degree'] = len(self.solution['multiplicity']['result']) - 1
            self.solution['multiplicity']['degree_text'] = f"The degree of the polynomial " \
                f"{self.get_horn_scheme_string(self.solution['gcd']['result'])}" \
                f" is: {self.solution['multiplicity']['degree']}."
        else:
            self.solution['multiplicity']['result'] = self.solution['gcd']['result']
            self.solution['multiplicity']['text'] += f"{self.solution['multiplicity']['result'][0]}, " \
                f"which is a simple constant, " \
                f"thus the poly P doesn't have any multiplicity roots greater than 1."
            self.coefficient_list = self.read_poly_from_source()
        self.solution['halley']['vector'] = []
        self.solution['halley']['iter'] = 0
        self.solution['halley']['text'] = "Halley's method for computing the roots of the poly: "

        while len(self.solution['halley']['vector']) < self.poly_degree:
            start_x = self.get_x_start(self.coefficient_list)
            passed_x_list = [start_x]
            x_nr, status = self.halley_method(self.coefficient_list, start_x)
            self.solution['halley']['iter'] += 1

            while status is False:
                start_x = self.get_x_start(self.coefficient_list)

                if start_x in passed_x_list:
                    continue
                else:
                    passed_x_list.append(start_x)
                    x_nr, status = self.halley_method(self.coefficient_list, start_x)
                    self.solution['halley']['iter'] += 1

            found = False

            if any(abs(solution - x_nr) <= self.epsilon['high'] for solution in self.solution['halley']['vector']):
                found = True

            if found is False:
                self.solution['halley']['vector'].append(x_nr)

        self.solution['halley']['vector'] = sorted(self.solution['halley']['vector'])
        self.solution['halley']['text'] += f"The algorithm completed in {self.solution['halley']['iter']} iterations." \
            f" The roots of the polynomial are: {self.solution['halley']['vector']}"
        self.solution['elapsed'] = time.time() - self.solution['elapsed']
        self.solution['expiration'] = time.time() + 900
        self.solution['output'] = dump_encoded_object(self.solution, indent=4, return_handler=True, set_filename=True)
        self.solution['file_name'] = self.solution['output'].name
        self.solution['output'].close()
        self.solution['output'] = None

        if print_it is True:
            print(json.dumps(self.solution, indent=4))

    def read_poly_from_source(self):
        if is_link(self.poly_file_input) is False:
            return read_poly_from_file(self.poly_file_input)
        else:
            # print('link')
            return read_poly_from_link(self.poly_file_input)

    @staticmethod
    def derive_poly(poly: list):
        derived_degree = len(poly) - 2
        derived_coefficients = [
            (derived_degree - idx + 1) * poly[idx] for idx in range(len(poly) - 1)
        ]
        return derived_coefficients, derived_degree

    @staticmethod
    def stringify_coefficient(coefficient):
        if coefficient < 0:
            return f"({coefficient})"

        return f"{coefficient}"

    def get_horn_scheme_string(self, poly: list = None):
        """

        :param poly: Optional parameter. Represents a custom list of coefficients of a poly.
                    If None it will provide take the list of coefficients from the first derivation of the base poly.
        :type poly: None or list
        :return: The decomposition of the poly based on Horner's scheme.
        :rtype: str
        """
        text = ""

        if poly is None:
            poly = self.coefficient_list

        for elm in poly[::-1]:

            if poly.index(elm) != 0:
                text = ") * x + " + self.stringify_coefficient(elm) + text
            else:
                text = '(' * (len(poly) - 2) + self.stringify_coefficient(elm) + text[1:]

        return text

    def compute_gcd(self, first_list: list = None, second_list: list = None):
        if first_list is None:
            first_list = [elm for elm in self.coefficient_list]

        if second_list is None:
            second_list = [elm for elm in self.derive_poly(self.coefficient_list)[0]]

        if len(second_list) == 0:
            return first_list

        new_divs = self.compute_euclid(first_list, second_list)
        return self.compute_gcd(second_list, new_divs[1])  # until only quotients are left

    def compute_euclid(self, first_list, second_list):
        # noinspection PyUnusedLocal
        quotients = [0 for counter in range(len(first_list))]
        remainders = first_list
        remainders_degree = len(remainders) - 1
        div_degree = len(second_list) - 1
        lead_coefficient_div = second_list[0]

        while remainders_degree >= div_degree:
            index = len(quotients) - (remainders_degree - div_degree) - 1
            quotients[index] = remainders[0] / lead_coefficient_div
            diff_len = len(second_list) + (remainders_degree - div_degree)
            # noinspection PyUnusedLocal
            diff = [0 for counter in range(diff_len)]

            for idx in range(len(second_list)):
                diff[idx] = remainders[0] / lead_coefficient_div * second_list[idx]

            for pos in range(len(diff)):
                remainders[pos] -= diff[pos]

            remainders = self.clean_poly(remainders)
            remainders_degree = len(remainders) - 1

        quotients = self.clean_poly(quotients)
        return quotients, remainders

    def clean_poly(self, poly):
        while len(poly) != 0 and abs(poly[0]) <= self.epsilon['high']:
            poly.pop(0)

        return poly

    def get_x_start(self, poly: list):
        edge = self.compute_real_number_edge(poly)

        return numpy.random.uniform(-1 * edge, edge + self.epsilon['highest'])

    @staticmethod
    def compute_real_number_edge(poly):
        return (abs(poly[0]) + max([abs(coefficient) for coefficient in poly])) / abs(poly[0])

    def halley_method(self, poly, start_x):
        count = 1
        x_nr = start_x
        poly_first_derived, first_derived_degree = self.derive_poly(poly)
        poly_second_derived, second_derived_degree = self.derive_poly(poly_first_derived)

        base_poly_result = self.compute_horn_scheme_result(self.coefficient_list, x_nr)
        first_derived_result = self.compute_horn_scheme_result(poly_first_derived, x_nr)
        second_derived_result = self.compute_horn_scheme_result(poly_second_derived, x_nr)

        a_max = at_pow(first_derived_result, 2) * 2 - base_poly_result * second_derived_result

        if abs(a_max) < self.epsilon['high']:
            # we need to break and change the value of the starting x
            return 0, False
        # our starting x is good enough to proceed further
        delta = base_poly_result * first_derived_result / a_max
        x_nr -= delta
        count += 1

        while self.epsilon['high'] <= abs(delta) <= self.epsilon['upper'] and count <= self.max_iter['halley']:
            poly_first_derived, first_derived_degree = self.derive_poly(poly)
            poly_second_derived, second_derived_degree = self.derive_poly(poly_first_derived)

            base_poly_result = self.compute_horn_scheme_result(self.coefficient_list, x_nr)
            first_derived_result = self.compute_horn_scheme_result(poly_first_derived, x_nr)
            second_derived_result = self.compute_horn_scheme_result(poly_second_derived, x_nr)

            a_max = at_pow(first_derived_result, 2) * 2 - base_poly_result * second_derived_result

            if abs(a_max) < self.epsilon['high']:
                # we need to break and change the value of the starting x
                return 0, False

            # our starting x is good enough to proceed further
            delta = base_poly_result * first_derived_result / a_max
            x_nr -= delta
            count += 1

        # Delta check
        if abs(delta) < self.epsilon['high']:
            return x_nr, True
        else:
            return 0, False

    @staticmethod
    def compute_horn_scheme_result(poly, x_nr):
        idx = 0
        result = poly[idx]

        for idx in range(idx + 1, len(poly)):
            result = poly[idx] + result * x_nr

        return result


if __name__ == '__main__':
    a_poly_solver = PolySolver('polinom.txt')
    a_poly_solver.solve()
