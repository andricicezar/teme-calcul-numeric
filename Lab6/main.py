from flask_cors import CORS
from werkzeug.datastructures import ImmutableMultiDict
from Lab6.utils.utils import *
from Lab6.tema6 import PolySolver
from flask import Flask, request, redirect, render_template, jsonify

app = Flask(
    __name__,
    # os.path.abspath(os.path.join(os.curdir, 'public')),
    # static_folder='public',
    # template_folder=os.path.abspath(os.path.join(os.curdir, 'templates'))
)

settings = settings_init()
app.template = dict(
    subtitle='Tema 6 - Bugeag George Tiberiu & Andrici Cezar Constantin',
    post_address=settings['address'] + ':' + settings['port']
)
app.template_folder = os.path.join(app.root_path, settings['paths']['templates'])
app.static_folder = os.path.join(app.root_path, settings['paths']['static'])
CORS(app)


def clean_expired_saves():
    save_files_path = os.path.join(app.root_path, settings['paths']['saves'])
    for file in os.listdir(os.path.join(app.root_path, settings['paths']['saves'])):
        if file.endswith('.json'):
            with open(os.path.join(save_files_path, file), 'r') as fh:
                file_dict = json.load(fh)
            if file_dict.get('expiration') is None or file_dict['expiration'] < time.time():
                os.unlink(os.path.join(save_files_path, file))


@app.context_processor
def inject_template_rendered():
    clean_expired_saves()
    return dict(template=app.template or dict(
        name='base.html',
        subtitle='Tema 6 - Bugeag George Tiberiu & Andrici Cezar Constantin',
        post_address=settings['address'] + ':' + settings['port']
    ))


@app.route('/')
@app.route('/index')
def redirect_to_home():
    return redirect('/home')


@app.route('/problema1', methods=['GET'])
def problema1():
    app.template['name'] = 'problema1.html'
    return render_template(app.template['name'])


def parse_values(form):
    if form['type'] == 'CREATE':
        if 'coefficients' not in form:
            raise ValueError('The list of coefficients was not passed!')
        values = {elm: form[elm] for elm in ImmutableMultiDict.to_dict(form, flat=False)}
        try:
            values['coefficients'] = [float(eval(elm)) for elm in values['coefficients'].split(', ')]
        except SyntaxError:
            raise ValueError('The list of coefficients inputted is invalid!')
        return values
    elif form['type'] == 'UPLOAD':
        print(dict(form))
        return form
    elif form['type'] == 'READ_FILE':
        return form
    elif form['type'] == 'READ_LINK':
        return form
    else:
        raise ValueError('This type of polynomial input is not supported or missing!')


@app.route('/problema1/solution', methods=['POST'])
def solve_poly():
    if 'type' not in request.form:
        return jsonify({'error': 'Invalid request! Polynomial input type not specified!'}), 400
    try:
        values = parse_values(request.form)
        if values['type'] == 'CREATE':
            write_poly_to_file(settings['paths']['poly'], values['coefficients'], mode='w+')
            a_poly_solver = PolySolver(settings['paths']['poly'])
            a_poly_solver.solve(print_it=False)
            return jsonify(a_poly_solver.solution), 200
        else:
            print(values)
            pass
    except ValueError as e:
        print(e)
        return jsonify({'error': 'Invalid request! ' + str(e)}), 400
    return jsonify(values), 201


@app.route('/home')
def home_page():
    app.template['name'] = 'index.html'
    return render_template(app.template['name'])


@app.route('/poly', methods=['GET'])
def get_polynomial():
    values = request.form
    if 'file_name' not in values:
        return jsonify(dict(error='Invalid request! No file was specified!')), 400
    with open(values['file_name'], 'r') as fh:
        context = fh.read()
        return context, 200


@app.route('/polynomial.txt', methods=['GET'])
def serve_polynomial():
    try:
        with open(
            os.path.normpath(
                os.path.abspath(
                    settings['paths']['poly']
                )
            ),
            'r+'
        ) as fh:
            content = fh.read()
        app.template['name'] = 'file_content.html'
        return render_template(app.template['name'], content=content), 200
    except FileNotFoundError as e:
        app.template['name'] = 'error.html'
        print(e)
        return render_template(
            app.template['name'], status_code='500', custom_message='The requested polynomial file could not be found!'
        ), 500


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('-a', '--host', type=str, help='Address(IP/DNS) at which the node will be found')
    parser.add_argument('-p', '--port', type=int, help='Port on which the node will listen')
    args = parser.parse_args()
    host = args.host or settings['address']
    port = args.port or settings['port']
    app.run(host=host, port=port, debug=True, threaded=True)
