# import operator
# import os
# from functools import reduce
# import numpy as np
# import math
# import random
# from utils.utils import at_pow
import urllib
import time
from urllib import request


class RareMatrix:
    def __init__(self, N, checkIfRare = True):
        self.N = N
        self.checkIfRare = checkIfRare
        self.ds = [[]] * N

    def insert(self, i, j, v):
        if v == 0:
            raise Exception("Zeros are not accepted")

        if len(self.ds[i]) == 0:
            self.ds[i] = [(j, v)]
            return 0

        if len(self.ds[i]) == 12 and self.checkIfRare:
            raise Exception("Too many elements on line " + str(i))

        for k in range(0, len(self.ds[i])):
            if self.ds[i][k][0] == j:
                self.ds[i][k] = (j, self.ds[i][k][1] + v)
                return
            elif j < self.ds[i][k][0]:
                self.ds[i] = self.ds[i][:k] + [(j, v)] + self.ds[i][k:]
                return k

        self.ds[i].append((j, v))

    def append(self, i, j, v):
        if len(self.ds[i]) == 0:
            self.ds[i] = [(j,v)]
        else:
            self.ds[i].append((j,v))

    def equals(self, B):
        A = self.ds
        B = B.matrix.ds

        for l in range(0, self.N):
            a = len(A[l])

            if a != len(B[l]):
                raise Exception("Matrixes are not equal")

            for i in range(0, a):
                if A[l][i][0] != B[l][i][0] or not equals(A[l][i][1], B[l][i][1]):
                    return False

        return True


def equals(a, b):
    return abs(a-b) < (10 ** -8)


class CustomMatrix:
    def __init__(self, link, checkIfRare = True):
        print("Loading " + link)
        response = urllib.request.urlopen(link).readlines()
        self.b = []
        state = 0
        for rawLine in response:
            line = rawLine.decode('utf-8').strip()

            if state == 0:
                self.matrix = RareMatrix(int(line), checkIfRare)
                state = 1
            elif state == 1:
                # empty line
                state = 2
            elif state == 2 and len(line) > 0:
                v, i, j = map(lambda x: x.strip(), line.split(','))
                self.matrix.insert(int(i), int(j), float(v))
            elif state == 2:
                # empty line
                state = 3
            elif state == 3 and len(line) > 0:
                self.b.append(float(line))
            elif state == 3:
                state = 4

        self.checkIfInitializedCorrectly()

    def checkIfInitializedCorrectly(self):
        # self.matrix * (2019, ... 1)^T = self.b

        s = [0] * 2019
        for l in range(0, self.matrix.N):
            s[l] = 0
            for el in self.matrix.ds[l]:
                if l == 0:
                    print(l, el[0], el[1])
                s[l] += el[1] * (2019 - el[0]) 
        print(s[:10])
        return
        
            # if not equals(s, self.b[l]):
            #     raise Exception("the matrix has been read incorrectly")

    def add(self, B):
        R = RareMatrix(self.matrix.N)
        A = self.matrix.ds
        B = B.matrix.ds

        for l in range(0, self.matrix.N):
            i = 0
            j = 0

            a = len(A[l])
            b = len(B[l])

            while j < b or i < a:
                if j >= b or (i < a and A[l][i][0] < B[l][j][0]):
                    R.append(l, A[l][i][0], A[l][i][1])
                    i += 1
                elif i >= a or (j < b and A[l][i][0] > B[l][j][0]):
                    R.append(l, B[l][j][0], B[l][j][1])
                    j += 1
                elif A[l][i][0] == B[l][j][0]:
                    R.append(l, A[l][i][0], A[l][i][1] + B[l][j][1])
                    i += 1
                    j += 1

        return R

    def multiply(self, B):
        R = RareMatrix(self.matrix.N, False)
        A = self.matrix.ds
        B = B.matrix.ds

        for la in range(0, self.matrix.N):
            for lb in range(0, self.matrix.N):
                for a in A[la]:
                    for b in B[lb]:
                        if a[0] == lb:
                            R.insert(la, b[0], a[1] * b[1])

        return R

    def scalar(self, x):
        R = RareMatrix(self.matrix.N, False)
        A = self.matrix.ds

        for la in range(0, self.matrix.N):
            for a in A[la]:
                R.append(la, a[0], a[1] * x)

        return R


class Problem:
    def __init__(self, linka, linkb, linkaplusb, linkaorb):
        self.linka = linka
        self.linkb = linkb
        self.linkaplusb = linkaplusb
        self.linkaorb = linkaorb

    def solve(self):
        A = CustomMatrix(self.linka)
        B = CustomMatrix(self.linkb)
        APLUSB = CustomMatrix(self.linkaplusb, False)
        AORIB = CustomMatrix(self.linkaorb, False)

        result = dict()

        start_time = time.time()
        aplusb = A.add(B)
        result["aplusb_time"] = "%.15f sec(s)" % (time.time() - start_time)
        result["aplusb_correct"] = aplusb.equals(APLUSB)

        start_time = time.time()
        aorib = A.multiply(B)
        result["aorib_time"] = "%.15f sec(s)" % (time.time() - start_time)
        result["aorib_correct"] = aorib.equals(AORIB)

        start_time = time.time()
        ax = A.scalar(7)
        result["ax_time"] = "%.15f sec(s)" % (time.time() - start_time)

        return result


if __name__ == "__main__":
    A = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/3/a.txt")
    # B = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/3/b.txt")
    # APLUSB = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/3/aplusb.txt", False)
    # AORIB = CustomMatrix("https://profs.info.uaic.ro/~ancai/CN/lab/3/aorib.txt", False)

    aplusb = A.add(B)
    aorib = A.multiply(B)
