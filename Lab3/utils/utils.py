def at_pow(nr, power):
    if power < 0:
        return 0
    if power == 0:
        return 1
    if power == 1:
        return nr
    if power == 2:
        return nr * nr
    if power > 2:
        # print(f"{nr} la puterea {power}")
        if power % 2 == 0:
            return at_pow(at_pow(nr, power // 2), 2)
        if power % 2 == 1:
            return at_pow(at_pow(nr, power // 2), 2) * nr


def settings_init():
    return dict(
        paths=dict(
            templates='templates',
            static='public'
        ),
        address='127.0.0.1',
        port='27000',
        protocol='http://',
    )
