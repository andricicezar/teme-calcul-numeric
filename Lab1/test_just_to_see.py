import os
import random
import time

import numpy as np
import math


def generate_elements(min_max_tuple, nr_of_elm):
    # start_time = time.time()
    random.seed(os.urandom(128))
    np.random.seed(random.randint(0, nr_of_elm))
    result = np.random.uniform(
        low=min_max_tuple[0],
        high=min_max_tuple[1] + 0.000000000000001,
        size=nr_of_elm
    )
    # print(f"Time to gen elements was: {time.time() - start_time} sec(s)")
    return result.tolist(), nr_of_elm


def fol_for_normal(lista_nr):
    result = []
    for nr_x in lista_nr:
        result.append(1 + nr_x * (-1 + nr_x))
    return result


def fol_list_comprh(lista_nr):
    return [1 + nr_x * (-1 + nr_x) for nr_x in lista_nr]


nr_values = 10_000_000
min_max_range = tuple([-(math.pi / 2), math.pi / 2])
# start_time = time.time()
all_points_list, nr_values = generate_elements(min_max_range, nr_values or 100_000)
start_time = time.time()
result_for = fol_for_normal(all_points_list)
print("Timp estimat pt for normal este %.15f sec(s)" % (time.time() - start_time))
start_time = time.time()
result_comprh = fol_for_normal(all_points_list)
print("Timp estimat pt list comprehension este %.15f sec(s)" % (time.time() - start_time))



