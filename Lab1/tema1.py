import operator
import os
from functools import reduce
import numpy as np
import time
import math
import random
from utils.utils import at_pow


def first_problem():
    """
        1. Sa se gas. cel mai mare nr
            u = 10**-m a.i. 1+[c] u = 1 unde +[c] inseamna operatie facuta de calculator
                2**-p
                u - precizia masina
    """

    def assert_u_truth(nr_u):
        return 1 + nr_u != 1

    m = 1
    for m in range(1000000000000000000000000):
        u = 10 ** -m
        if not assert_u_truth(u):
            break
    u_to_print = f"0.{'0' * (m - 2)}1"

    return dict(
        message="Precision: {0}\n10^(-{1}) (at power -m)".format(u_to_print, m - 1),
        data=dict(
            u_precision_good=u_to_print,
            m_good=m - 1,
            u_false=f"0.{'0' * (m - 1)}1",
            m_false=m
        )
    )


def second_prbl():
    """
        2. a + b + c as (a + b) + c sau a + (b + c)
            (1.0 + u) + u | 1.0 + (u + u)
    """

    def assert_assoc_truth(a, b, c):
        return (a * b) * c == a * (b * c)
    random.seed(os.urandom(1024), 2)
    a_1 = random.random()
    b_1 = random.random()
    c_1 = random.random()
    tries = 1
    while assert_assoc_truth(a_1, b_1, c_1):
        random.seed(os.urandom(1024), 2)
        a_1 = random.random()
        b_1 = random.random()
        c_1 = random.random()
        if tries == 5000000:
            break
        else:
            tries += 1

    return dict(
        message="(A + B) + C = A + (B + C)",
        dict=dict(
            a=f"{a_1}",
            b=f"{b_1}",
            c=f"{c_1}",
            tries=f"{tries}",
            result=f"{assert_assoc_truth(a_1, b_1, c_1)}"
        )
    )


def third_prbl(nr_values=None):
    def get_constants_table(constants, printed=False):
        table = ""
        header = (
            f"| Direct[Index]{' ' * (38 - len('Direct[Index]'))} vs"
            f" {' ' * (37 - len('Computed[Index]'))}Computed[Index]"
            f" | Same?  |"
        )
        row_line = len(header) * "-"
        header = row_line + '\n' + header + '\n' + row_line
        table += '\n' + header
        if printed:
            print(header)
        for const_idx in range(1, 7):
            string = "| [%d]:%.32f | [%d]:%.32f | %s |" % (
                const_idx, constants['direct'][f"{const_idx}"],
                const_idx, constants['computed'][f"{const_idx}"],
                "True  " if constants['direct'][f"{const_idx}"] == constants['computed'][f"{const_idx}"] else "False "
            )
            table += '\n' + string + '\n' + row_line
            if printed:
                print(string, row_line, sep='\n')
        return table

    def init_consts():
        return {
            "direct": {
                "1": 0.16666666666666666666666666666667,
                "2": 0.00833333333333333333333333333333,
                "3": 1.984126984126984126984126984127e-4,
                "4": 2.7557319223985890652557319223986e-6,
                "5": 2.5052108385441718775052108385442e-8,
                "6": 1.6059043836821614599392377170155e-10
            },
            "computed": {
                "1": 1 / reduce(operator.mul, range(1, 4), 1),
                "2": 1 / reduce(operator.mul, range(1, 6), 1),
                "3": 1 / reduce(operator.mul, range(1, 8), 1),
                "4": 1 / reduce(operator.mul, range(1, 10), 1),
                "5": 1 / reduce(operator.mul, range(1, 12), 1),
                "6": 1 / reduce(operator.mul, range(1, 14), 1)
            }
        }

    def generate_elements(min_max_tuple, nr_of_elm):
        random.seed(os.urandom(128))
        np.random.seed(random.randint(0, nr_of_elm))
        result = np.random.uniform(
            low=min_max_tuple[0],
            high=min_max_tuple[1] + 0.000000000000001,
            size=nr_of_elm
        )
        return result.tolist(), nr_of_elm

    def polinom_one(list_of_nr_x, const, optimal=True):
        result = []
        if not optimal:
            for nr_x in list_of_nr_x:
                result.append(
                    nr_x - const['direct']['1'] * at_pow(nr_x, 3) +
                    const['direct']['2'] * at_pow(nr_x, 5)
                )
        else:
            for nr_x in list_of_nr_x:
                nr_x_at_2 = nr_x * nr_x
                result.append(
                    nr_x * (
                        1 + nr_x_at_2 * (
                            -const['direct']['1'] + const['direct']['2'] * nr_x_at_2
                        )
                    )
                )
        return result

    def polinom_two(list_of_nr_x, const, optimal=True):
        result = []
        if not optimal:
            for nr_x in list_of_nr_x:
                result.append(
                    nr_x - const['direct']['1'] * at_pow(nr_x, 3) +
                    const['direct']['2'] * at_pow(nr_x, 5) -
                    const['direct']['3'] * at_pow(nr_x, 7)
                )
        else:
            for nr_x in list_of_nr_x:
                nr_x_at_2 = nr_x * nr_x
                result.append(
                    nr_x * (
                        1 + nr_x_at_2 * (
                            -const['direct']['1'] + nr_x_at_2 * (
                                const['direct']['2'] - const['direct']['3'] * nr_x_at_2
                            )
                        )
                    )
                )
        return result

    def polinom_three(list_of_nr_x, const, optimal=True):
        result = []
        if not optimal:
            for nr_x in list_of_nr_x:
                result.append(
                    nr_x - const['direct']['1'] * at_pow(nr_x, 3) +
                    const['direct']['2'] * at_pow(nr_x, 5) -
                    const['direct']['3'] * at_pow(nr_x, 7) +
                    const['direct']['4'] * at_pow(nr_x, 9)
                )
        else:
            for nr_x in list_of_nr_x:
                nr_x_at_2 = nr_x * nr_x
                result.append(
                    nr_x * (
                        1 + nr_x_at_2 * (
                            -const['direct']['1'] + nr_x_at_2 * (
                                const['direct']['2'] + nr_x_at_2 * (
                                    -const['direct']['3'] + const['direct']['4'] * nr_x_at_2
                                )
                            )
                        )
                    )
                )
        return result

    def polinom_four(list_of_nr_x, const, optimal=True):
        result = []
        if not optimal:
            for nr_x in list_of_nr_x:
                result.append(
                    nr_x - 0.166 * at_pow(nr_x, 3) + 0.00833 * at_pow(nr_x, 5) -
                    const['direct']['3'] * at_pow(nr_x, 7) + const['direct']['4'] * at_pow(nr_x, 9)
                )
        else:
            for nr_x in list_of_nr_x:
                nr_x_at_2 = nr_x * nr_x
                result.append(
                    nr_x * (1 + nr_x_at_2 * (
                        -0.166 + nr_x_at_2 * (
                            0.00833 + nr_x_at_2 * (
                                -const['direct']['3'] + const['direct']['4'] * nr_x_at_2
                                )
                            )
                        )
                    )
                )
        return result

    def polinom_five(list_of_nr_x, const, optimal=True):
        result = []
        if not optimal:
            for nr_x in list_of_nr_x:
                result.append(
                    nr_x - const['direct']['1'] * at_pow(nr_x, 3) + const['direct']['2'] * at_pow(nr_x, 5) -
                    const['direct']['3'] * at_pow(nr_x, 7) + const['direct']['4'] * at_pow(nr_x, 9) -
                    const['direct']['5'] * at_pow(nr_x, 11)
                )
        else:
            for nr_x in list_of_nr_x:
                nr_x_at_2 = nr_x * nr_x
                result.append(
                    nr_x * (1 + nr_x_at_2 * (
                        -const['direct']['1'] + nr_x_at_2 * (
                            const['direct']['2'] + nr_x_at_2 * (
                                -const['direct']['3'] + nr_x_at_2 * (
                                    const['direct']['4'] - const['direct']['5'] * nr_x_at_2
                                    )
                                )
                            )
                        )
                    )
                )
        return result

    def polinom_six(list_of_nr_x, const, optimal=True):
        result = []
        if not optimal:
            for nr_x in list_of_nr_x:
                result.append(
                    nr_x - const['direct']['1'] * at_pow(nr_x, 3) + const['direct']['2'] * at_pow(nr_x, 5) -
                    const['direct']['3'] * at_pow(nr_x, 7) + const['direct']['4'] * at_pow(nr_x, 9) -
                    const['direct']['5'] * at_pow(nr_x, 11) + const['direct']['6'] * at_pow(nr_x, 13)
                )
        else:
            for nr_x in list_of_nr_x:
                nr_x_at_2 = nr_x * nr_x
                result.append(
                    nr_x * (1 + nr_x_at_2 * (
                        -const['direct']['1'] + nr_x_at_2 * (
                            const['direct']['2'] + nr_x_at_2 * (
                                -const['direct']['3'] + nr_x_at_2 * (
                                    const['direct']['4'] + nr_x_at_2 * (
                                        -const['direct']['5'] + const['direct']['6'] * nr_x_at_2
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
        return result

    def sinus_of_x(list_of_nr_x):
        return [math.sin(nr_x) for nr_x in list_of_nr_x]

    def compute_values_in_points(fction_to_use, list_of_points, constants=None):
        result = dict(
            p_values=[],
            start_time=None,
            end_time=None,
        )
        result['start_time'] = time.time()
        if not constants:
            result['p_values'] = fction_to_use(list_of_points)
        else:
            result['p_values'] = fction_to_use(list_of_points, constants)
        result['end_time'] = time.time()
        return result

    def get_err_diff(tested_list, exact_list):
        if len(tested_list) != len(exact_list):
            raise ValueError("Lists must be of same size!")
        return [abs(tested_list[index] - exact_list[index]) for index in range(0, len(tested_list))]

    consts = init_consts()
    min_max_range = tuple([-(math.pi / 2), math.pi / 2])
    all_points_list, nr_values = generate_elements(min_max_range, nr_values or 100_000)
    full_result = dict(
        place_1=dict(),
        place_2=dict(),
        place_3=dict(),
        place_4=dict(),
        place_5=dict(),
        place_6=dict(),
        sin=dict()
    )
    computed_results = []
    used_functions = {
        0: sinus_of_x,
        1: polinom_one,
        2: polinom_two,
        3: polinom_three,
        4: polinom_four,
        5: polinom_five,
        6: polinom_six,
        'e': get_err_diff
    }
    computed_results.append(compute_values_in_points(used_functions[0], all_points_list))
    computed_results[0]['poly_nr'] = 0
    computed_results[0]['err_nr'] = 0
    for idx in range(1, 7):
        computed_results.append(compute_values_in_points(used_functions[idx], all_points_list, consts))
        computed_results[idx]['err_nr'] = sum(
            1 for i, j in zip(computed_results[idx]['p_values'], computed_results[0]['p_values']) if i != j
        )
        computed_results[idx]['poly_nr'] = f"{idx}"
    computed_results = sorted(computed_results, key=operator.itemgetter('err_nr'))
    for idx in range(0, 7):
        if idx == 0:
            full_result['sin']['poly_nr'] = 'Sin(x)'
            full_result['sin']['err_nr'] = 0
            full_result['sin']['e_rate'] = "  0.00%"
            full_result['sin']['time'] = f"%.15f" % (
                computed_results[idx]['end_time'] -
                computed_results[idx]['start_time']
            )
        else:
            full_result[f'place_{idx}']['poly_nr'] = computed_results[idx]['poly_nr']
            full_result[f'place_{idx}']['err_nr'] = computed_results[idx]['err_nr']
            full_result[f'place_{idx}']['e_rate'] = \
                f"% 3.2f%%" % float(computed_results[idx]['err_nr'] * 100 / nr_values)
            full_result[f'place_{idx}']['time'] = f"%.15f" % (
                computed_results[idx]['end_time'] -
                computed_results[idx]['start_time']
            )
    return dict(result=full_result, const_table=get_constants_table(consts))
