import time

from flask import Flask, request, redirect, render_template, jsonify
from flask_cors import CORS
from utils.utils import settings_init
from tema1 import first_problem, second_prbl, third_prbl
import os

app = Flask(
    __name__,
    # os.path.abspath(os.path.join(os.curdir, 'public')),
    # static_folder='public',
    # template_folder=os.path.abspath(os.path.join(os.curdir, 'templates'))
)

settings = settings_init()
app.template = dict()
app.template_folder = os.path.join(app.root_path, settings['paths']['templates'])
app.static_folder = os.path.join(app.root_path, settings['paths']['static'])
CORS(app)


@app.context_processor
def inject_template_rendered():
    return dict(template=app.template or dict(name='base.html'))


@app.route('/')
@app.route('/index')
def redirect_to_home():
    return redirect('/home')


@app.route('/problema1/solution', methods=['POST'])
def problema_unu_solution():
    print("A fost facut request pentru exercitiul 1:")
    start_time = time.time()
    print("Start request at: %s" % time.strftime('%Y-%m-%d %H:%M:%S %p', time.localtime(start_time)))
    try:
        result = first_problem()
        print("Timp executie request solutie exercitiu 1: %.15f sec(s)" % (time.time() - start_time))
        return jsonify(result), 201
    except ValueError as e:
        print("Timp executie request solutie exercitiu 1: %.15f sec(s)" % (time.time() - start_time))
        return str(e), 400


@app.route('/problema1', methods=['GET'])
def problema_unu_page():
    app.template = dict(name='problema1.html')
    return render_template(app.template['name'])


@app.route('/problema2/solution', methods=['POST'])
def problema_doi_solution():
    print("A fost facut request pentru exercitiul 2:")
    start_time = time.time()
    print("Start request at: %s" % time.strftime('%Y-%m-%d %H:%M:%S %p', time.localtime(start_time)))
    try:
        values = request.form
        if 'tries' not in values:
            return "Input number of tries!", 400
        try:
            nr_tries = int(values['tries'])
        except Exception as e:
            print(e)
            print("Timp executie request solutie exercitiu 2: %.15f sec(s)" % (time.time() - start_time))
            raise ValueError("Invalid input!")
        result = []
        while nr_tries > 0:
            result.append(second_prbl())
            nr_tries -= 1
        print("Timp executie request solutie exercitiu 2: %.15f sec(s)" % (time.time() - start_time))
        return jsonify(dict(data=result)), 201
    except ValueError as e:
        print("Timp executie request solutie exercitiu 2: %.15f sec(s)" % (time.time() - start_time))
        return str(e), 400


@app.route('/problema2', methods=['GET'])
def problema_doi_page():
    app.template = dict(name='problema2.html')
    return render_template(app.template['name'])


@app.route('/problema3/solution', methods=['POST'])
def problema_trei_solution():
    print("A fost facut request pentru exercitiul 3:")
    start_time = time.time()
    print("Start request at: %s" % time.strftime('%Y-%m-%d %H:%M:%S %p', time.localtime(start_time)))
    try:
        values = request.form
        if 'nr_values' not in values:
            print("Timp executie request solutie exercitiu 3: %.15f sec(s)" % (time.time() - start_time))
            return "Input number of values to be generated!", 400
        try:
            nr_values = int(values['nr_values'])
        except Exception as e:
            print(e)
            print("Timp executie request solutie exercitiu 3: %.15f sec(s)" % (time.time() - start_time))
            raise ValueError("Invalid input!")
        if nr_values > 50_000_000:
            nr_values = 50_000_000
        result = list(third_prbl(nr_values).get('result').values())
        # result = [elm for elm in req_result.values()]
        print("Timp executie request: %.15f sec(s)" % (time.time() - start_time))
        return jsonify(result), 201
    except ValueError as e:
        print("Timp executie request solutie exercitiu 3: %.15f sec(s)" % (time.time() - start_time))
        print(e)
        return str(e), 400


@app.route('/problema3', methods=['GET'])
def problema_trei_page():
    app.template = dict(name='problema3.html')
    return render_template(app.template['name'])


@app.route('/home')
def home_page():
    app.template = dict(name='index.html')
    return render_template(app.template['name'])


# @app.errorhandler(400)
# def bad_request():
#     return "Bad request!", 400


# @app.route('/renderer/error')
# def render_error():
#     value = request.form['data'] or None
#     if not value:
#         return "Bad request!", 400
#     app.template = dict(name='error.html')
#     full_path = os.path.abspath(os.path.curdir)
#     try:
#         with (os.path.abspath(os.path.join(full_path, app.template['name'])), 'r+') as fh:
#             fh.write(value)
#         return render_template(app.template['name'])
#     except IOError as e:
#         print(e)
#         try:
#             file_name = 'err_' + time.strftime('%Y_%m(%B)_%d_%H_%M_%S_%p') + '.html'
#             with (os.path.abspath(os.path.join(full_path, file_name)), 'r+') as fh:
#                 fh.write(value)
#             return render_template(file_name)
#         except IOError as e:
#             print(e)
#             raise


def clean_up():
    dir_files = os.listdir(app.template_folder)
    allowed_templates = [
        'base.html',
        'error.html',
        'index.html',
        'problema1.html',
        'problema2.html',
        'problema3.html'
    ]
    try:
        [
            os.unlink(os.path.abspath(os.path.join(app.template_folder, file)))
            for file in dir_files if file not in allowed_templates
        ]
        return
    except Exception as e:
        print(e)
        pass


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('-a', '--host', type=str, help='Address(IP/DNS) at which the node will be found')
    parser.add_argument('-p', '--port', type=int, help='Port on which the node will listen')
    args = parser.parse_args()
    host = args.host or settings['address']
    port = args.port or settings['port']
    app.run(host=host, port=port, debug=True, threaded=True)
